package com.snippets.pageobjects;

import library.auto.SeleniumFuncLibrary;

import org.openqa.selenium.By;


public class GoogleSearchPO {
	
	private final SeleniumFuncLibrary selenium;
	
	private By searchTextboxBy = By.xpath(".//input[@id='gs_htif0']");
	
	private By toolsQALinkBy = By.xpath(".//*[@id='rso']/div[1]/div/div/h3/a");
	
	public GoogleSearchPO(SeleniumFuncLibrary selenium) {
		this.selenium = selenium;
	}

	public void enterSearchText(String text) {
		selenium.type(text, new By[]{ searchTextboxBy });
	}
	
	public void clickSearch()
	{
		selenium.click(new By[]{ toolsQALinkBy });
	}
}
