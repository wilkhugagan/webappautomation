package com.snippets.viewobjects;

import java.util.Date;
import java.util.HashMap;

import library.vo.ParentVO;

public class CustomerVO implements ParentVO {
	public String firstName;
	public String lastName;
	public Date dob;
	public String age;
	public String address1;
	public boolean married;

	@Override
	public HashMap<String, Object> listAllFields() {
		HashMap<String, Object> list = new HashMap<String, Object>();
		
		list.put("FirstName", firstName);
		list.put("LastName", lastName);
		list.put("DOB", dob);
		list.put("Age", age);
		list.put("Address", address1);
		list.put("Married", married);
		
		return list;
	}
}
