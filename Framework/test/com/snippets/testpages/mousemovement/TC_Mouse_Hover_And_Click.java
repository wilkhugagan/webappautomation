package com.snippets.testpages.mousemovement;

import org.openqa.selenium.By;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import library.auto.SeleniumFuncLibrary;
import library.auto.contants.TimeUnits;

public class TC_Mouse_Hover_And_Click {
	SeleniumFuncLibrary selenium;

	@BeforeMethod
	public void setUp() {
		selenium = new SeleniumFuncLibrary("FIREFOX");
		selenium.launchBrowser("http://www.flipkart.com/");
	}

	@Test
	public void test() {
		// Moving cursor to Category 'Men'
		selenium.moveCursorTo(
				new By[] { By.xpath(".//*[@id='container']/div/div/header/div[1]/div/ul/li[2]/a/span[2]") });

		selenium.click(new By[]{ By.xpath(".//span[contains(text(), 'Split ACs')]") });
	}

	@AfterMethod
	public void tearDown() {
		selenium.pause(10, "Pausing to see results before browser is closed.", TimeUnits.SECONDS);
		selenium.closeBrowser();
	}
}
