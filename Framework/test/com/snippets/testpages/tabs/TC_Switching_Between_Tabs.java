package com.snippets.testpages.tabs;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import library.auto.SeleniumFuncLibrary;
import library.auto.contants.TimeUnits;

public class TC_Switching_Between_Tabs {
	SeleniumFuncLibrary selenium;
	@BeforeMethod
	public void setUp() throws Exception {
		selenium = new SeleniumFuncLibrary("FIREFOX");
		selenium.launchBrowser("http://www.google.com/");
	}


	@Test
	public void test() {
		selenium.openNewTab("http://keep.google.com");
		selenium.openNewTab("https://en.wikipedia.org");
		
		selenium.switchToTabForwardBy(1);
		
		selenium.switchToTabForwardBy(1);
	}


	@AfterMethod
	public void tearDown() throws Exception {
		selenium.pause(10, "Pausing to see results before browser is closed.", TimeUnits.SECONDS);	
		selenium.closeBrowser();
	}

}
