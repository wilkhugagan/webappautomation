package com.snippets.testpages.tabs;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import library.auto.SeleniumFuncLibrary;
import library.auto.contants.TimeUnits;

public class TC_Creating_And_Handling_New_Tab {
	SeleniumFuncLibrary selenium;
	@BeforeMethod
	public void setUp() throws Exception {
		selenium = new SeleniumFuncLibrary("FIREFOX");
		selenium.launchBrowser("http://www.toolsqa.com/");
	}


	@Test
	public void test() {
		selenium.openNewTab("http://www.gmail.com");
	}


	@AfterMethod
	public void tearDown() throws Exception {
		selenium.pause(10, "Pausing to see results before browser is closed.", TimeUnits.SECONDS);
		selenium.closeBrowser();
	}

}
