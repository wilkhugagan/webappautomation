package com.snippets.testpages.pageobjects;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.snippets.pageobjects.GoogleSearchPO;

import library.auto.SeleniumFuncLibrary;
import library.auto.contants.TimeUnits;

public class GoogleSearchTest {
	SeleniumFuncLibrary selenium;
	GoogleSearchPO googleSearch;
	
	@BeforeClass
	public void before() {
		selenium = new SeleniumFuncLibrary("FIREFOX");		
		selenium.launchBrowser("http://www.google.co.in");
		
		googleSearch = new GoogleSearchPO(selenium);
	}
	
	@Test
	public void test() {
		googleSearch.enterSearchText("facebook login");		
		googleSearch.clickSearch();
	}
	
	@AfterClass
	public void after() {
		selenium.pause(10, "", TimeUnits.MILLISECONDS);
		
		selenium.closeBrowser();
	}
}
