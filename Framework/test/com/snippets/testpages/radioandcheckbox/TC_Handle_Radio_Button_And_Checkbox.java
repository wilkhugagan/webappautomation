package com.snippets.testpages.radioandcheckbox;

import org.openqa.selenium.By;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import library.auto.SeleniumFuncLibrary;
import library.auto.contants.TimeUnits;

public class TC_Handle_Radio_Button_And_Checkbox {
	SeleniumFuncLibrary selenium;

	@BeforeMethod
	public void setUp() throws Exception {
		selenium = new SeleniumFuncLibrary("CHROME");
		selenium.launchBrowser("http://toolsqa.com/automation-practice-form");
	}

	@Test
	public void test() {
		
		// Select the Sex: Female radio button
		selenium.click(new By[]{ By.xpath("//input[@id='sex-1']") });
		
		// Select Years of Exp: 3 radio button
		selenium.click(new By[]{ By.xpath("//input[@id='exp-2']") });
		
		selenium.type("05/06/2016", new By[]{ By.id("datepicker") });
		
		// Tick the checkbox Profession: Automation Tester
		selenium.click(new By[]{ By.cssSelector("input[id='profession-1']") });
		
		// Tick the checkbox Automation Tool: Selenium Webdriver
		selenium.click(new By[]{ By.cssSelector("input[id='tool-2']") });
	}

	@AfterMethod
	public void tearDown() throws Exception {
		selenium.pause(10, "Pausing to see results before browser is closed.", TimeUnits.SECONDS);
		selenium.closeBrowser();
	}
}
