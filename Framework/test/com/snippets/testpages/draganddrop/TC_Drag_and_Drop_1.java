package com.snippets.testpages.draganddrop;

import org.openqa.selenium.By;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import library.auto.SeleniumFuncLibrary;
import library.auto.contants.TimeUnits;

public class TC_Drag_and_Drop_1 {
	SeleniumFuncLibrary selenium;
	
	@BeforeMethod
	public void setUp() throws Exception {
		selenium = new SeleniumFuncLibrary("FIREFOX");
		selenium.launchBrowser("http://www.dhtmlx.com/docs/products/dhtmlxTree/index.shtml");
	}


	@Test
	public void test() {
		String forElement = ".//*[@id='treebox1']/div/table/tbody/tr[2]/td[2]/table/tbody/tr[2]/td[2]/table/tbody/tr[1]/td[4]/span";
		String toDrop = ".//*[@id='treebox2']/div/table/tbody/tr[2]/td[2]/table/tbody/tr[2]/td[2]/table/tbody/tr[2]/td[2]/table/tbody/tr[1]/td[4]/span";
		selenium.dragAndDrop(new By[]{ By.xpath(forElement) }, new By[]{ By.xpath(toDrop) });
	}


	@AfterMethod
	public void tearDown() throws Exception {
		selenium.pause(10, "Pausing to see results before browser is closed.", TimeUnits.SECONDS);
		selenium.closeBrowser();
	}

}
