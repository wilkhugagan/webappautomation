package com.snippets.testpages.excelreadorwrite;

import java.util.Date;

import org.testng.annotations.Test;

import com.snippets.viewobjects.CustomerVO;

import library.datasource.DataSourceFactory;
import library.datasource.DataSourceType;
import library.datasource.ExcelDataSource;
import library.datasource.connectiondescriptors.ExcelConnectionDescriptor;
import library.exceptions.TransportFailureException;


public class TC_Write_VO_To_Excel_Row {

	@Test(groups = {"ExcelWriteTest"})
	public void test() throws TransportFailureException {
		
		CustomerVO custVo = new CustomerVO();
		
		custVo.firstName = "Gagandeep";
		custVo.lastName = "Singh";
		custVo.dob = new Date();
		custVo.age = "25";
		custVo.address1 = "Shahdara";
		
		String excelFilePath = System.getProperty("user.dir") + "\\data\\excel\\CustomerDetails.xlsx";
		ExcelConnectionDescriptor con = new ExcelConnectionDescriptor(excelFilePath, "Sheet1", 10, 0, 3);
		
		ExcelDataSource dataSource = (ExcelDataSource) DataSourceFactory.getDataSource(DataSourceType.EXCEL);
		
		dataSource.save(custVo, con);
	}

}
