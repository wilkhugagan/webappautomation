package com.snippets.testpages.excelreadorwrite;

import org.testng.annotations.Test;

import com.snippets.viewobjects.CustomerVO;

import library.datasource.DataSourceFactory;
import library.datasource.DataSourceType;
import library.datasource.ExcelDataSource;
import library.datasource.connectiondescriptors.ExcelConnectionDescriptor;
import library.exceptions.TransportFailureException;


public class TC_Read_Excel_Row_To_VO{

	@Test(groups = {"ExcelReadTest"}, dependsOnGroups = {"ExcelWriteTest"})
	public void test() throws TransportFailureException {
		ExcelDataSource dataSouce = (ExcelDataSource) DataSourceFactory.getDataSource(DataSourceType.EXCEL);
		
		String excelFilePath = System.getProperty("user.dir") + "\\data\\excel\\CustomerDetails.xlsx";
		
		CustomerVO customerVO = new CustomerVO();
		
		dataSouce.load(customerVO, new ExcelConnectionDescriptor(excelFilePath, "Sheet1", 0, 100, 2));
	}

}
