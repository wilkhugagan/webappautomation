package com.snippets.testpages.excelreadorwrite;

import java.io.IOException;
import java.util.HashMap;

import library.datasource.connectiondescriptors.ExcelConnectionDescriptor;
import library.engines.ExcelSourceEngine;
import library.exceptions.excel.InvalidExcelCellStateException;
import library.exceptions.excel.InvalidExcelRowException;

import org.testng.annotations.Test;

public class TC_Read_Row_From_Excel {

	@Test(groups = {"ExcelReadTest"}, dependsOnGroups = {"ExcelWriteTest"})
	public void test() throws IOException, InvalidExcelCellStateException, InvalidExcelRowException {
		
		String excelFilePath = System.getProperty("user.dir") + "\\data\\excel\\CustomerDetails.xlsx";
		ExcelConnectionDescriptor con = new ExcelConnectionDescriptor(excelFilePath, "Sheet1", 0, 0, 1);
		
		HashMap<String, Object> data = ExcelSourceEngine.loadData(con);
		for(String key : data.keySet()) {
			System.out.println(key + " : " + data.get(key));
		}
	}

}
