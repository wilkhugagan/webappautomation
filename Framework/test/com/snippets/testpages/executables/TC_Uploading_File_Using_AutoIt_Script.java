package com.snippets.testpages.executables;

import library.auto.SeleniumFuncLibrary;
import library.auto.ExeFuncLibrary;
import library.auto.contants.Browsers;
import library.auto.contants.TimeUnits;
import library.exceptions.CommandFailureException;

import org.openqa.selenium.By;
import org.testng.annotations.Test;

public class TC_Uploading_File_Using_AutoIt_Script {

	@Test(groups = {"FileUpload"})
	public void test() throws CommandFailureException {
		SeleniumFuncLibrary selenium = new SeleniumFuncLibrary("FIREFOX");

		selenium.launchBrowser("http://toolsqa.com/automation-practice-form/");

		selenium.click(new By[]{ By.id("photo") });

		String pathToExecutable = null;

		if (selenium.currentBrowser == Browsers.CHROME) {
			pathToExecutable = System.getProperty("user.dir") + "/autoitscripts/Chrome/FileUpload.exe";
		}
		else if (selenium.currentBrowser == Browsers.FIREFOX) {
			pathToExecutable = System.getProperty("user.dir") + "/autoitscripts/Firefox/FileUpload.exe";
		}

		ExeFuncLibrary.runExecutable(pathToExecutable, new String[] { "c:\\logs", "selenium.log" });

		selenium.pause(5, "Waiting before closing browser", TimeUnits.SECONDS);

		selenium.closeBrowser();
	}
}
