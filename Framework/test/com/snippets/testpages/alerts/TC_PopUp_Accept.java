package com.snippets.testpages.alerts;

import org.openqa.selenium.By;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import library.auto.SeleniumFuncLibrary;
import library.auto.contants.TimeUnits;

public class TC_PopUp_Accept {

	SeleniumFuncLibrary selenium;


	@BeforeMethod
	public void setUp() throws Exception {
		selenium = new SeleniumFuncLibrary("FIREFOX");
		selenium.launchBrowser("http://toolsqa.com/automation-practice-switch-windows/");
	}


	@Test
	public void test() {

		selenium.click(new By[]{ By.xpath("//button[contains(text(),'Alert Box')]")});
		
		selenium.switchToPopUp();

		selenium.popUpAccept();
	}


	@AfterMethod
	public void tearDown() throws Exception {
		selenium.pause(10, "Pausing to see results before browser is closed.", TimeUnits.SECONDS);

		selenium.closeBrowser();
	}

}
