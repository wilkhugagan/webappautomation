package com.snippets.testpages.propertiesreadorwrite;

import org.testng.annotations.Test;

import com.snippets.viewobjects.CustomerVO;

import library.datasource.DataSourceFactory;
import library.datasource.DataSourceType;
import library.datasource.PropertiesDataSource;
import library.datasource.connectiondescriptors.PropertiesConnectionDescriptor;
import library.exceptions.TransportFailureException;

public class TC_Write_VO_To_Properties_File {

	@Test(groups = {"PropertiesWriteTest"})
	public void test() throws TransportFailureException {
		PropertiesDataSource dataSource = (PropertiesDataSource) DataSourceFactory.getDataSource(DataSourceType.PROPERTIES_FILE);

		CustomerVO custVo = new CustomerVO();
		custVo.firstName = "Gagan deep";
		custVo.lastName = "Singh";

		String propFilePath = System.getProperty("user.dir") + "\\data\\properties\\Customer.properties";
		dataSource.save(custVo, new PropertiesConnectionDescriptor(propFilePath));
		
	}

}
