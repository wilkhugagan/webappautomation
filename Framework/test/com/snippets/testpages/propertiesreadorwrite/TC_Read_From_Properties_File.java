package com.snippets.testpages.propertiesreadorwrite;

import org.testng.annotations.Test;

import com.snippets.viewobjects.CustomerVO;

import library.datasource.DataSourceFactory;
import library.datasource.DataSourceType;
import library.datasource.PropertiesDataSource;
import library.datasource.connectiondescriptors.PropertiesConnectionDescriptor;
import library.exceptions.TransportFailureException;


public class TC_Read_From_Properties_File {

	@Test(groups = {"PropertiesReadTest"})
	public void test() throws TransportFailureException {
		PropertiesDataSource dataSource = (PropertiesDataSource) DataSourceFactory.getDataSource(DataSourceType.PROPERTIES_FILE);
		
		CustomerVO custVo = new CustomerVO();
		
		String propFilePath = System.getProperty("user.dir") + "\\data\\properties\\Customer.properties";
		dataSource.load(custVo, new PropertiesConnectionDescriptor(propFilePath));
		
	}

}
