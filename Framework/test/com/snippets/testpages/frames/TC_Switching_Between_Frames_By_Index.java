package com.snippets.testpages.frames;

import library.auto.SeleniumFuncLibrary;
import library.auto.contants.TimeUnits;

import org.openqa.selenium.By;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class TC_Switching_Between_Frames_By_Index {
	SeleniumFuncLibrary selenium;

	@BeforeMethod
	public void setUp() throws Exception {
		selenium.launchBrowser("http://toolsqa.com/iframe-practice-page/");
	}

	@Test
	public void test() {
		selenium.switchToFrame(0);

		selenium.type("Gagandeep", new By[] { By.name("firstname") });

		selenium.resetFocus();

		selenium.switchToFrame(1);

		selenium.click(new By[] { By.xpath("//a[contains(@href,'pattern-14.png')]") });
	}

	@AfterMethod
	public void tearDown() throws Exception {
		selenium.pause(10, "Pausing to see results before browser is closed.", TimeUnits.SECONDS);
		selenium.closeBrowser();
	}

}
