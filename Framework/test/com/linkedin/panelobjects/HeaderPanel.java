package com.linkedin.panelobjects;

import org.openqa.selenium.By;

import library.auto.SeleniumFuncLibrary;
import library.logger.MessageLogger;

/**
 * This class represents the Header panel which appears on the top of each and
 * every page when user is logged in.
 * 
 * @author Gagandeep Singh
 */
public class HeaderPanel {
	private static final MessageLogger log = new MessageLogger(HeaderPanel.class);
	private SeleniumFuncLibrary selenium;

	// Element identifiers
	private static final By home_Link_XPath = By.xpath("//*[@id='feed-nav-item']/a");
	private static final By myNetwork_Link_XPath = By.xpath("//*[@id='mynetwork-nav-item']/a");
	private static final By jobs_Link_XPath = By.xpath("//*[@id='jobs-nav-item']/a");
	private static final By messaging_Link_XPath = By.xpath("//*[@id='messaging-nav-item']/a");
	private static final By notification_Link_XPath = By.xpath("//*[@id='notifications-nav-item']/a");
	// End

	public HeaderPanel(SeleniumFuncLibrary selenium) {
		this.selenium = selenium;
	}

	public boolean clickMyNetworkLink() {
		log.info("Clicking 'My Network' link");
		return selenium.click(new By[] { myNetwork_Link_XPath });
	}
}
