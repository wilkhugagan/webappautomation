package com.linkedin.panelobjects.connections;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

import org.openqa.selenium.By;

import library.auto.SeleniumFuncLibrary;
import library.logger.MessageLogger;

/**
 * This class represents the new connection cards which appears in the "People
 * You May Know" page.
 * 
 * @author Gagandeep Singh
 */
class NewConnectionCardWrapperPanel {
	private static final MessageLogger log = new MessageLogger(NewConnectionCardWrapperPanel.class);
	private SeleniumFuncLibrary selenium;

	// Element identifiers
	private final By[] cardIdentifier;
	// Elements under this card
	private static final By personName_Span_XPath = By.xpath("//span[contains(@class, 'person-info__name')]");
	private static final By personInfoOrOccupation_Span_XPath = By.xpath("//span[contains(@class, 'person-info__occupation')]");
	private static final By connect_Button_ByXPath = By.xpath("//div[contains(@class, 'person-card__card-actions')]/button[1]");
	private static final By dismiss_Button_ByXPath = By.xpath("//div[contains(@class, 'person-card__card-actions')]/button[2]");

	/**
	 * @param selenium
	 * @param cardIdentifier
	 *            {@link org.openqa.selenium.By By} object of the element that
	 *            contains all the cards.
	 */
	public NewConnectionCardWrapperPanel(SeleniumFuncLibrary selenium, By[] cardIdentifier) {
		this.cardIdentifier = cardIdentifier;
		this.selenium = selenium;
	}

	public String getPersonName() {
		log.info("Searching person's name. (" + personName_Span_XPath + ")");
		
		ArrayList<By> array = new ArrayList<By>();
		array.addAll((Collection<? extends By>) Arrays.asList(cardIdentifier));
		array.add(personName_Span_XPath);

		return selenium.getText(array.toArray(new By[] {}));		
	}
	
	public boolean clickConnectButton() {
		log.info("Clicking the 'Connect' button. (" + connect_Button_ByXPath + ")");

		ArrayList<By> array = new ArrayList<By>();
		array.addAll((Collection<? extends By>) Arrays.asList(cardIdentifier));
		array.add(connect_Button_ByXPath);

		return selenium.click(array.toArray(new By[] {}));
	}

	public boolean clickDismissButton() {
		log.info("Clicking the 'Dismiss' button. (" + dismiss_Button_ByXPath + ")");

		ArrayList<By> array = new ArrayList<By>();
		array.addAll((Collection<? extends By>) Arrays.asList(cardIdentifier));
		array.add(dismiss_Button_ByXPath);

		return selenium.click(array.toArray(new By[] {}));
	}

	public boolean ifInfoOrOccupationLabelContains(String text) {
		log.info("Checking if text (" + text + ") exits in the info label. (" + personInfoOrOccupation_Span_XPath + ")");

		ArrayList<By> array = new ArrayList<By>();
		array.addAll((Collection<? extends By>) Arrays.asList(cardIdentifier));
		array.add(personInfoOrOccupation_Span_XPath);

		return selenium.textExist(text, array.toArray(new By[] {}));
	}
}
