package com.linkedin.panelobjects.connections;

import org.openqa.selenium.By;

import library.auto.SeleniumFuncLibrary;
import library.logger.MessageLogger;

/**
 * This class represents the panel/div that contains the
 * {@link com.linkedin.panelobjects.connections.NewConnectionCardWrapperPanel
 * NewConnectionCardWrapperPanel}
 * 
 * @author Gagandeep Singh
 */
public class NewConnectionCardContainerPanel {
	private static final MessageLogger log = new MessageLogger(NewConnectionCardContainerPanel.class);
	private SeleniumFuncLibrary selenium;

	// Element Identifiers
	private static final By connectionCard_Section_XPath = By.xpath("//section[contains(@class,'pymk-list')]");

	public NewConnectionCardContainerPanel(SeleniumFuncLibrary selenium) {
		this.selenium = selenium;
	}

	private NewConnectionCardWrapperPanel getConnectionCard(int cardNumber) {
		log.info("Creating a new Connection Card instance");
		return new NewConnectionCardWrapperPanel(selenium,
				new By[] { connectionCard_Section_XPath, By.xpath("//ul/li[" + cardNumber + "]") });
	}

	/**
	 * Gets you the name of the person.
	 * @param cardNumber
	 * @return
	 */
	public String getPersonName(int cardNumber) {
		return getConnectionCard(cardNumber).getPersonName();
	}
	
	/**
	 * Clicks the card's "Connect" button.
	 * 
	 * @param cardNumber
	 *            Number of the card. Starts from 1.
	 */
	public boolean clickConnectButtonOfCard(int cardNumber) {
		return getConnectionCard(cardNumber).clickConnectButton();
	}

	/**
	 * Clicks the card's "Dismiss" button.
	 * 
	 * @param cardNumber
	 *            Number of the card. Starts from 1.
	 */
	public boolean clickDismissButtonOfCard(int cardNumber) {
		return getConnectionCard(cardNumber).clickDismissButton();
	}

	/**
	 * Checks if specified text exists in the info/occupation label below
	 * person's name.
	 * 
	 * @param cardNumber
	 *            Number of the card. Starts from 1.
	 * @param text
	 *            Text to look for
	 * @return true if text found. false otherwise.
	 */
	public boolean ifPersonInfoOrOccupationContains(int cardNumber, String text) {
		return getConnectionCard(cardNumber).ifInfoOrOccupationLabelContains(text);
	}
}
