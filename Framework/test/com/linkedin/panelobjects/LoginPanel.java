package com.linkedin.panelobjects;

import org.openqa.selenium.By;

import library.auto.SeleniumFuncLibrary;
import library.logger.MessageLogger;

public class LoginPanel {
	private static final MessageLogger log = new MessageLogger(LoginPanel.class);
	private SeleniumFuncLibrary selenium = null;	
	
	// Element identifiers
	private static final By id_ById = By.id("login-email"), 
			id_XPath = By.xpath("//input[@class='login-email' and @id='login-email']");
	private static final By pass_ById = By.id("login-password"), 
			pass_XPath = By.xpath("//input[@class='login-password' and @id='login-password']");
	private static final By loginButton_ById = By.id("login-submit"), 
			loginButton_ByXPath = By.xpath("//input[@class='login submit-button' and @id='login-submit']");
	
	public LoginPanel(SeleniumFuncLibrary selenium) {
		this.selenium = selenium;
	}
	
	/**
	 * @param id : User email/phone
	 */
	public void enterLoginId(String id) {
		log.info("Entering login id: " + id);
		
		if (!selenium.type(id, new By[]{ id_ById })) {
			selenium.type(id, new By[]{ id_XPath });
		}
	}
	
	/**
	 * @param pass : Password
	 */
	public void enterLoginPassword(String pass) {
		log.info("Entering login password: " + pass);
		
		if (!selenium.type(pass, new By[]{ pass_ById })) {
			selenium.type(pass, new By[]{ pass_XPath });
		}
	}
	
	public boolean clickLoginButton() {
		log.info("Clicking the login button");
		
		if (selenium.click(new By[]{ loginButton_ById })) {
			return true;
		}
		else {
			return selenium.click(new By[]{ loginButton_ByXPath });
		}
	}
}
