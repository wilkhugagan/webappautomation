package com.linkedin.externalreferences;

/**
 * Contains paths to the external files. Data, config, etc.
 * This will help if base path of file name changes. No other changes will be required to the scripts.
 * Just change the path/name of the file here and done.
 * 
 * @author Gagandeep Singh
 */
public class ExcelFiles {
	// Base directory for all the files.
	private static final String EXCEL_FILE_BASE_DIRECTORY = "D:\\Dev\\Workspace\\repositories\\webappautomation\\Framework\\data\\excel\\LinkedIn";	
	// File paths relative to base path specified above
	public static final String ENVIRONMENT_DETAILS_FILE = EXCEL_FILE_BASE_DIRECTORY + "\\EnvironmentDetails.xlsx";
	public static final String ACCOUNT_DETAILS_FILE = EXCEL_FILE_BASE_DIRECTORY + "\\Accounts\\AccountDetails.xlsx";
	public static final String CONNECTIONS_FILE = EXCEL_FILE_BASE_DIRECTORY + "\\Connections\\Connections.xlsx";
	
	/**
	 * This class can be used to store name of the sheets in above excel file.
	 * To access the sheet name use: ExcelFiles.ExcelSheet.SheetVariableName
	 * 
	 * @author Gagandeep Singh
	 */
	public static class ExcelSheets {
		// ACCOUNT_DETAILS_FILE sheets
		public static final String ACCOUNT_DETAILS_SHEET_NAME = "Account Details";
		public static final int ACCOUNT_DETAILS_SHEET_INDEX = 0;
		
		// ENVIRONMENT_DETAILS_FILE sheets
		public static final String ENVIRONMENT_DETAILS_SHEET_NAME = "Environment Details";
		public static final int ENVIRONMENT_DETAILS_SHEET_INDEX = 0;
		
		// CONNECTIONS_FILE sheets
		public static final String NEW_CONNECTIONS_SHEET_NAME = "Requested To Connect";
		public static final int NEW_CONNECTIONS_SHEET_INDEX = 0;
	}
}
