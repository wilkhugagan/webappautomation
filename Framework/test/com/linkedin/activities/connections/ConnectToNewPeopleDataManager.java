package com.linkedin.activities.connections;

import com.linkedin.externalreferences.ExcelFiles;
import com.linkedin.viewobjects.AccountDetailsVO;
import com.linkedin.viewobjects.ConnectionVO;
import com.linkedin.viewobjects.EnvironmentDetailsVO;

import library.datasource.DataSourceFactory;
import library.datasource.DataSourceType;
import library.datasource.ExcelDataSource;
import library.datasource.connectiondescriptors.ExcelConnectionDescriptor;
import library.exceptions.TransportFailureException;
import library.logger.MessageLogger;

public class ConnectToNewPeopleDataManager {
	private static final MessageLogger log = new MessageLogger(ConnectToNewPeopleDataManager.class);
	private static ExcelDataSource dataSource = (ExcelDataSource) DataSourceFactory.getDataSource(DataSourceType.EXCEL);
	private static final AccountDetailsVO accountDetailsVO = new AccountDetailsVO();
	private static final EnvironmentDetailsVO envirDetailsVO = new EnvironmentDetailsVO();

	// Data loading
	/**
	 * Loads data from external data file. Create and keep VO objects for the
	 * data loaded.
	 * 
	 * @throws TransportFailureException
	 */
	public static void loadData(String suiteName) throws TransportFailureException {
		ExcelConnectionDescriptor conDesc;

		log.info("Loading the environment details...");
		conDesc = new ExcelConnectionDescriptor(ExcelFiles.ENVIRONMENT_DETAILS_FILE,
				ExcelFiles.ExcelSheets.ENVIRONMENT_DETAILS_SHEET_NAME,
				ExcelFiles.ExcelSheets.ENVIRONMENT_DETAILS_SHEET_INDEX, 0, 1);

		boolean testSuiteMatched = false;
		// Loading value until find a row for the specified test suite name
		for (int i = 1; i < dataSource.getRowCount(conDesc); i++) {
			conDesc.setDataRowIndex(i);
			dataSource.load(envirDetailsVO, conDesc);

			if (suiteName.equalsIgnoreCase(envirDetailsVO.testSuite)) {
				testSuiteMatched = true;
				break;
			}
		}

		if (testSuiteMatched) {
			log.info("Row found for test suite : '" + suiteName + "'");
		} else {
			throw new TransportFailureException("No row for test suite (" + suiteName + ") exists.");
		}

		log.info("Loading the account details...");
		conDesc = new ExcelConnectionDescriptor(ExcelFiles.ACCOUNT_DETAILS_FILE,
				ExcelFiles.ExcelSheets.ACCOUNT_DETAILS_SHEET_NAME, ExcelFiles.ExcelSheets.ACCOUNT_DETAILS_SHEET_INDEX,
				0, 1);
		dataSource.load(accountDetailsVO, conDesc);
	}

	public static AccountDetailsVO getAccountDetailsVO() {
		return accountDetailsVO;
	}

	public static EnvironmentDetailsVO getEnvDetailsVO() {
		return envirDetailsVO;
	}

	// Data storing
	public static boolean storeNewConnectionDetails(ConnectionVO connectionVo) throws TransportFailureException {
		ExcelConnectionDescriptor conDesc;

		log.info("Storing new connection details...");
		conDesc = new ExcelConnectionDescriptor(ExcelFiles.CONNECTIONS_FILE,
				ExcelFiles.ExcelSheets.NEW_CONNECTIONS_SHEET_NAME, ExcelFiles.ExcelSheets.NEW_CONNECTIONS_SHEET_INDEX,
				0);
		return dataSource.save(connectionVo, conDesc);
	}
}
