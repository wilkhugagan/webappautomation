package com.linkedin.activities.connections;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.linkedin.panelobjects.HeaderPanel;
import com.linkedin.panelobjects.LoginPanel;
import com.linkedin.panelobjects.connections.NewConnectionCardContainerPanel;
import com.linkedin.viewobjects.AccountDetailsVO;
import com.linkedin.viewobjects.ConnectionVO;
import com.linkedin.viewobjects.EnvironmentDetailsVO;

import library.auto.SeleniumFuncLibrary;
import library.auto.contants.TimeUnits;
import library.exceptions.TransportFailureException;
import library.logger.MessageLogger;

public class ConnectToNewPeopleImpl {
	private static final MessageLogger log = new MessageLogger(ConnectToNewPeopleImpl.class);
	private static SeleniumFuncLibrary selenium;
	private static AccountDetailsVO accountDetailsVO = new AccountDetailsVO();
	private static EnvironmentDetailsVO envirDetailsVO = new EnvironmentDetailsVO();

	@Parameters(value = { "SuiteName" })
	@BeforeTest(groups = { "NewConnection" })
	public void setup(String suiteName) {
		log.info("Start : beforeTest (" + this.getClass() + ")");
		try {
			log.info("Loading data...");
			ConnectToNewPeopleDataManager.loadData(suiteName);

			envirDetailsVO = ConnectToNewPeopleDataManager.getEnvDetailsVO();
			accountDetailsVO = ConnectToNewPeopleDataManager.getAccountDetailsVO();

		} catch (TransportFailureException e) {
			log.error("Unable to load data from external file.", e);
			Assert.fail("Unable to load data from external file.");
		}

		if (envirDetailsVO.checkIfBrowserNameEmpty()) {
			log.error("No browser name specified for the test suite: " + envirDetailsVO.testSuite);
			Assert.fail("No browser name specified for the test suite: " + envirDetailsVO.testSuite);
		}

		if (envirDetailsVO.checkIfURLEmpty()) {
			log.error("Application URL missing");
			Assert.fail("Application URL missing");
		}

		// Creating the selenium object
		selenium = new SeleniumFuncLibrary(envirDetailsVO.browserName);

		log.info("Complete : Login (" + this.getClass() + ")");
	}

	@Test(groups = { "NewConnection" })
	public void login() {
		log.info("Start : Login (" + this.getClass() + ")");

		selenium.launchBrowser(envirDetailsVO.url);

		LoginPanel loginPanel = new LoginPanel(selenium);

		if (accountDetailsVO.email == null) {
			log.error("Email is missing");
			Assert.assertNotNull(accountDetailsVO.email, "Email is missing");
		}
		loginPanel.enterLoginId(accountDetailsVO.email);

		if (accountDetailsVO.password == null) {
			log.error("Password is missing");
			Assert.assertNotNull(accountDetailsVO.password, "Password is missing");
		}
		loginPanel.enterLoginPassword(accountDetailsVO.password);

		if (loginPanel.clickLoginButton()) {
			log.info("Re-directing to Home page.");
		} else {
			log.error("Error while submitting login button.");
			Assert.fail("Error while submitting login button.");
		}

		log.info("Complete : Login (" + this.getClass() + ")");
	}

	@Test(dependsOnMethods = { "login" }, groups = { "NewConnection" })
	public void moveToMyNetworkPage() {
		log.info("Start : moveToMyNetworkPage (" + this.getClass() + ")");

		HeaderPanel headerPanel = new HeaderPanel(selenium);
		if (headerPanel.clickMyNetworkLink()) {
			log.info("Re-directing to My Network page.");
		} else {
			log.error("Error while re-directing to My Network page.");
			Assert.fail("Error while re-directing to My Network page.");
		}

		log.info("Complete : moveToMyNetworkPage (" + this.getClass() + ")");
	}

	/**
	 * This method will perform actions to connect to people who do not belong
	 * to Sopra Steria
	 */
	@Test(dependsOnMethods = { "moveToMyNetworkPage" }, groups = { "NewConnection" })
	public void connectToFirstCardThatAppears() {
		log.info("Start : connectToFirstCardThatAppears (" + this.getClass() + ")");

		NewConnectionCardContainerPanel card = new NewConnectionCardContainerPanel(selenium);

		for (int round = 1; round <= 10; round++) {
			for (int i = 1; i <= 3; i++) {
				selenium.pause(4, "Giving time to connection cards to rearrange themselves", TimeUnits.SECONDS);
				if (!card.ifPersonInfoOrOccupationContains(1, "sopra")) {

					ConnectionVO connectionVo = new ConnectionVO();
					connectionVo.personName = card.getPersonName(1);

					log.info("Connecting to '" + connectionVo.personName + "'");
					if (card.clickConnectButtonOfCard(1)) {
						log.info("Connected to '" + connectionVo.personName + "' now.");
						try {
							ConnectToNewPeopleDataManager.storeNewConnectionDetails(connectionVo);
						} catch (TransportFailureException e) {
							log.error("Error while trying to store the new connection details", e);
							Assert.fail("Error while trying to store the new connection details", e);
						}
					} else {
						log.error("Error while adding a new connection.");
						Assert.fail("Error while adding a new connection.");
					}

				} else {
					log.info("Dismissing " + card.getPersonName(1) + "'s connection card.");
					if (card.clickDismissButtonOfCard(1)) {
						log.info("Dismissed " + card.getPersonName(1) + "'s connection card.");
					} else {
						log.error("Error while dismissing " + card.getPersonName(1) + "'s connection card.");
						Assert.fail("Error while adding a new connection.");
					}
				}
			}
			selenium.refreshPage();
		}

		log.info("Complete : connectToFirstCardThatAppears (" + this.getClass() + ")");
	}

	@AfterTest(groups = { "NewConnection" })
	public void tearDown() throws InterruptedException {
		log.info("Start : afterTest (" + this.getClass() + ")");
		selenium.closeBrowser();

		log.info("Start : afterTest (" + this.getClass() + ")");
	}
}
