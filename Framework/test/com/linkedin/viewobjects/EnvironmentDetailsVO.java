package com.linkedin.viewobjects;

import java.util.HashMap;

import library.vo.ParentVO;

public class EnvironmentDetailsVO implements ParentVO {
	public String testSuite;
	public String browserName;
	public String url;
	
	public boolean checkIfBrowserNameEmpty() {
		if (browserName == null || browserName.equalsIgnoreCase("")) {
			return true;
		}
		else
			return false;
	}
	
	public boolean checkIfURLEmpty() {
		if (url == null || url.equalsIgnoreCase("")) {
			return true;
		}
		else
			return false;
	}

	@Override
	public HashMap<String, Object> listAllFields() {
		HashMap<String, Object> list = new HashMap<String, Object>();

		list.put("Test Suite", testSuite);
		list.put("Browser Name", browserName);
		list.put("URL", url);
		
		return list;
	}

}
