package com.linkedin.viewobjects;

import java.util.HashMap;

import library.vo.ParentVO;

public class ConnectionVO implements ParentVO {
	public String personName;

	@Override
	public HashMap<String, Object> listAllFields() {
		HashMap<String, Object> list = new HashMap<String, Object>();

		list.put("Person Name", personName);

		return list;
	}

}
