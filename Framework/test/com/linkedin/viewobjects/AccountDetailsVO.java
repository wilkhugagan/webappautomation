package com.linkedin.viewobjects;

import java.util.HashMap;

import library.vo.ParentVO;

public class AccountDetailsVO implements ParentVO {
	public String email;
	public String password;
	public String firstName;
	public String lastName;
	public String fullName;
		
	@Override
	public HashMap<String, Object> listAllFields() {
		HashMap<String, Object> list = new HashMap<String, Object>();
		
		list.put("Email", email);
		list.put("Password", password);
		list.put("First Name", firstName);
		list.put("Last Name", lastName);
		list.put("Full Name", fullName);

		return list;
	}

}
