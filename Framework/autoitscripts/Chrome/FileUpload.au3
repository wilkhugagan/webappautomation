; Note: Script only compatible with Chrome browser
; This script performs below tasks once Open Dialog window is open.
;	1. Go to the directory/folder(passed as Command Line parameter 1).
;	2. Input the file name(passed as Command Line parameter 2) in the File Name textbox to specify which file to upload.
;	3. Finally, simulates Enter key press to submit the file.
; Note: No handling is done if file doesn't exist in the specified folder.

#Region ;**** Directives created by AutoIt3Wrapper_GUI ****
#AutoIt3Wrapper_UseX64=y
#EndRegion ;**** Directives created by AutoIt3Wrapper_GUI ****
#region --- Au3Recorder generated code Start (v3.3.9.5 KeyboardLayout=00000409)  ---

#region --- Internal functions Au3Recorder Start ---
Func _Au3RecordSetup()
	Opt('WinWaitDelay',100)
	Opt('WinDetectHiddenText',1)
	Opt('MouseCoordMode',0)
	Local $aResult = DllCall('User32.dll', 'int', 'GetKeyboardLayoutNameW', 'wstr', '')
	If $aResult[1] <> '00000409' Then
		MsgBox(64, 'Warning', 'Recording has been done under a different Keyboard layout' & @CRLF & '(00000409->' & $aResult[1] & ')')
EndIf

EndFunc

Func _WinWaitActivate($title,$text,$timeout=0)
	WinWait($title,$text,$timeout)
	If Not WinActive($title,$text) Then WinActivate($title,$text)
	WinWaitActive($title,$text,$timeout)
EndFunc

_AU3RecordSetup()
#endregion --- Internal functions Au3Recorder End ---

_WinWaitActivate("Open","Namespace Tree Contr")
MouseClick("left",373,49,1)
Send($CmdLine[1] & "{ENTER}")
MouseClick("left",236,413,1)
Send($CmdLine[2] & "{ENTER}")
#endregion --- Au3Recorder generated code End ---