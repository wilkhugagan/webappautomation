package library.engines;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import library.datasource.DataSourceHelper;
import library.datasource.connectiondescriptors.ExcelConnectionDescriptor;
import library.exceptions.excel.InvalidExcelCellStateException;
import library.exceptions.excel.InvalidExcelRowException;
import library.logger.MessageLogger;

import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.util.WorkbookUtil;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFCreationHelper;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 * 
 * @author Gagandeep Singh
 *
 */
public class ExcelSourceEngine {

	private static final MessageLogger log = new MessageLogger(ExcelSourceEngine.class);

	private static FileInputStream fileInputStream = null;
	private static FileOutputStream fileOutputStream = null;
	private static XSSFWorkbook workbook = null;
	private static XSSFSheet sheet = null;
	// Object to be used by synchronized block
	private static String TOKEN = "TOKEN";

	private static final String ROW_CREATED_ON_COL_NAME = "ROW CREATED ON";

	/**
	 * Fetches a complete data row against the header row(specified by conDesc)
	 * from an excel sheet. <br>
	 * Note: This method is thread safe.
	 * 
	 * @param conDesc
	 *            {@link library.datasource.connectiondescriptors.ExcelConnectionDescriptor
	 *            ExcelConnectionDescriptor} instance that describes the file,
	 *            sheet and rows to read from.
	 * @return HashMap object containing Column Name, Column Value pair read
	 *         from excel sheet. Column names are normalized using
	 *         {@link library.datasource.DataSourceHelper#normalizeHeaderName(String)
	 *         normalizeHeaderName(String)} method.
	 * @throws IOException
	 * @throws InvalidExcelCellStateException
	 * @throws InvalidExcelRowException
	 */
	public static HashMap<String, Object> loadData(ExcelConnectionDescriptor conDesc)
			throws IOException, InvalidExcelCellStateException, InvalidExcelRowException {
		HashMap<String, Object> dataSet = new HashMap<String, Object>();

		log.debug("Connecting to excel file...");

		log.debug("Taking a lock on the excel file.");
		// So that only one thread at a time can access a file
		synchronized (TOKEN) {
			if (readExcelSheet(conDesc)) {

				int cellIndex = 0;

				try {
					log.debug("Fetching complete header row or row with the column names");
					// Represents row that contains column names
					XSSFRow headerRow = sheet.getRow(conDesc.getTableHeaderRowIndex());

					log.debug("Fetching the data row");
					// Represents row that contains column data
					XSSFRow dataRow = sheet.getRow(conDesc.getDataRowIndex());

					if (headerRow == null) {
						throw new InvalidExcelRowException("Header row you specified is empty", conDesc);
					} else if (dataRow == null) {
						throw new InvalidExcelRowException("Data row you specified is empty", conDesc);
					}

					int firstColIndex = headerRow.getFirstCellNum();
					int lastColIndex = headerRow.getLastCellNum();

					log.debug("It seems the data table starts from index: " + firstColIndex + " and ends at index: "
							+ lastColIndex);

					log.debug("Initiating data fetching...");
					// Reading and filling dataSet with Column Header, Data sets
					for (cellIndex = firstColIndex; cellIndex < lastColIndex; cellIndex++) {

						XSSFCell headerCell = headerRow.getCell(cellIndex);
						XSSFCell dataCell = dataRow.getCell(cellIndex);

						// Skip the columns that are blanks
						if (headerCell == null || headerCell.toString().trim() == "")
							continue;

						// To keep a check on the values used for header cell
						if (headerCell.getCellType() != XSSFCell.CELL_TYPE_STRING) {
							throw new InvalidExcelRowException(
									"It seems that you have added some invalid characters in your header cell. "
											+ "Please note that only Alphanumeric values, underscores(_), spaces( ) and hyphens(-) are allowed in the header row.",
									conDesc);
						}

						// Normalizing Header cell value
						String headerName = DataSourceHelper.normalizeHeaderName(headerCell.getStringCellValue());
						Object data = null; // Default is null if cell is blank

						if (dataCell != null) {
							// Parsing to different formats
							switch (dataCell.getCellType()) {

							case XSSFCell.CELL_TYPE_NUMERIC:

								// To check if it's a date. Excel stores date as
								// Double data type value.
								if (DateUtil.isCellDateFormatted(dataCell)) {
									data = dataCell.getDateCellValue();
								} else {
									// Changing the cell type to read values
									// like phone numbers, etc.
									dataCell.setCellType(XSSFCell.CELL_TYPE_STRING);
									data = dataCell.getStringCellValue();
								}

								break;

							case XSSFCell.CELL_TYPE_STRING:
								data = dataCell.getStringCellValue();
								break;

							case XSSFCell.CELL_TYPE_BOOLEAN:
								data = dataCell.getBooleanCellValue();
								break;

							case XSSFCell.CELL_TYPE_ERROR:
								throw new InvalidExcelCellStateException("Cell state is erroneous.", conDesc,
										dataCell.getColumnIndex());
							}
						}

						// Putting Column Header, Data in to DataSet
						dataSet.put(headerName, data);
					}
				} catch (InvalidExcelRowException e) {
					throw e;
				} catch (NullPointerException | IllegalStateException | InvalidExcelCellStateException e) {
					InvalidExcelCellStateException iere = new InvalidExcelCellStateException(e.getMessage(), conDesc,
							cellIndex);
					iere.setStackTrace(e.getStackTrace());

					log.error("", iere);

					throw iere;
				}

				closeExcelSheet(conDesc);

				log.debug("Releasing the lock on excel file");
			}
		}

		log.info("[ Data fetched from excel : " + dataSet + " ]");

		return dataSet;
	}

	/**
	 * Saves all the dataSet values to the excel sheet against the header
	 * columns(Specified by conDesc) for which there's a column mapped in the
	 * sheet. <br>
	 * Note: This method is thread safe.
	 * 
	 * @param dataSet
	 *            HashMap object containing Column Name, Column Value pair store
	 *            to excel sheet. Column names must be normalized using
	 *            {@link library.datasource.DataSourceHelper#normalizeHeaderName(String)
	 *            normalizeHeaderName(String)} method.
	 * @param conDesc
	 *            {@link library.datasource.connectiondescriptors.ExcelConnectionDescriptor
	 *            ExcelConnectionDescriptor} instance that describes the file,
	 *            sheet and rows to save to.
	 * @return true if data updated the sheet successfully. Otherwise throws one
	 *         of the exceptions below.
	 * @throws IOException
	 * @throws InvalidExcelCellStateException
	 * @throws InvalidExcelRowException
	 */
	public static boolean saveData(HashMap<String, Object> dataSet, ExcelConnectionDescriptor conDesc)
			throws IOException, InvalidExcelCellStateException, InvalidExcelRowException {

		log.info("Connecting to the excel file...");

		log.info("[ Storing data to excel : " + dataSet + " ]");

		// To track the columns that are not mapped
		ArrayList<String> columnsNotUpdated = new ArrayList<String>();
		// To preview the excel table update
		HashMap<String, Object> excelTable = new HashMap<String, Object>();

		log.debug("Taking a lock on the excel file.");
		// So that only one thread at a time can access a file
		synchronized (TOKEN) {
			// Reading the existing data from the excel sheet to append to it
			if (readExcelSheet(conDesc)) {

				int cellIndex = 0;

				log.debug("Fetching complete header row or row with the column names");
				// Represents row that contains column names
				XSSFRow headerRow = sheet.getRow(conDesc.getTableHeaderRowIndex());

				// Creating a new row at the end
				int newRowIndex = sheet.getLastRowNum() + 1;
				XSSFRow dataRow = sheet.getRow(newRowIndex);

				log.info("Creating a new row " + (newRowIndex));
				dataRow = sheet.createRow(newRowIndex);

				int firstColIndex = headerRow.getFirstCellNum();
				int lastColIndex = headerRow.getLastCellNum() - 1;

				try {

					// Assuming last column of the excel sheet is row creation
					// timestamp column
					int timeStampColIndex = lastColIndex;
					XSSFCell rowCreationTimeStampHeaderCell = headerRow.getCell(lastColIndex);
					XSSFCell rowCreationTimeStampDataCell = dataRow.getCell(lastColIndex);

					rowCreationTimeStampHeaderCell.setCellType(XSSFCell.CELL_TYPE_STRING);

					if (rowCreationTimeStampHeaderCell.getStringCellValue().equalsIgnoreCase(ROW_CREATED_ON_COL_NAME)) {
						// Ignoring row creation timestamp column as it's for
						// the use of system.
						lastColIndex--;
					} else {
						// If row creation timestamp column doesn't exist, it
						// will be created next to the last column
						timeStampColIndex = lastColIndex + 1;

						// Creating a header cell
						rowCreationTimeStampHeaderCell = headerRow.createCell(timeStampColIndex,
								XSSFCell.CELL_TYPE_STRING);
						rowCreationTimeStampHeaderCell.setCellValue(ROW_CREATED_ON_COL_NAME);
					}

					SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");

					// Creating timestamp cell
					rowCreationTimeStampDataCell = dataRow.createCell(timeStampColIndex, XSSFCell.CELL_TYPE_NUMERIC);
					rowCreationTimeStampDataCell.setCellValue(formatter.format(new Date()));

					log.debug("It seems the data table starts from index: " + firstColIndex + " and ends at index: "
							+ lastColIndex);

					// Creating cell style to format cell to date format
					XSSFCreationHelper createHelper = workbook.getCreationHelper();
					XSSFCellStyle cellStyle = workbook.createCellStyle();
					cellStyle.setDataFormat(createHelper.createDataFormat().getFormat("dd MMMM yyyy"));

					log.debug("Initiating data fetching...");
					// Reading and filling dataSet with Column Header, Data sets
					for (cellIndex = firstColIndex; cellIndex <= lastColIndex; cellIndex++) {

						XSSFCell headerCell = headerRow.getCell(cellIndex);
						// Creating a new cell as the new row is empty
						XSSFCell dataCell;

						// Skip the blank columns
						if (headerCell == null || headerCell.toString().trim() == "") {
							// Creating an empty cell just to avoid any format
							// issues later.
							dataRow.createCell(cellIndex);
							continue;
						}

						// To keep a check on the values used for header cell
						if (headerCell.getCellType() != XSSFCell.CELL_TYPE_STRING) {
							throw new InvalidExcelRowException(
									"It seems that you have added some invalid characters in your header cell. "
											+ "Please note that only Alphanumeric values, underscores(_), spaces( ) and hyphens(-) are allowed in the header row.",
									conDesc);
						}

						String normalisedHeaderCellName = DataSourceHelper
								.normalizeHeaderName(headerCell.getStringCellValue());

						// To track all the fields which are not updated in the
						// excel
						if (!dataSet.containsKey(normalisedHeaderCellName)) {
							columnsNotUpdated.add(headerCell.getStringCellValue());
						}

						Object data = dataSet.get(normalisedHeaderCellName);

						// Writing VO values to the excel filtered by their type
						if (data != null) {

							if (data.getClass().getSimpleName().equalsIgnoreCase("Date")) {
								dataCell = dataRow.createCell(cellIndex, XSSFCell.CELL_TYPE_NUMERIC);
								dataCell.setCellValue((Date) data);
								dataCell.setCellStyle(cellStyle);

								excelTable.put(headerCell.getStringCellValue(), dataCell.getDateCellValue());
							} else if (data.getClass().getSimpleName().equalsIgnoreCase("String")) {
								dataCell = dataRow.createCell(cellIndex, XSSFCell.CELL_TYPE_STRING);
								dataCell.setCellValue((String) data);

								excelTable.put(headerCell.getStringCellValue(), dataCell.getStringCellValue());
							} else if (data.getClass().getSimpleName().equalsIgnoreCase("Boolean")) {
								dataCell = dataRow.createCell(cellIndex, XSSFCell.CELL_TYPE_BOOLEAN);
								dataCell.setCellValue((boolean) data);

								excelTable.put(headerCell.getStringCellValue(), dataCell.getBooleanCellValue());
							}
						}
					}

				} catch (NullPointerException | IllegalStateException e) {
					InvalidExcelCellStateException iere = new InvalidExcelCellStateException(e.getMessage(), conDesc,
							cellIndex);
					iere.setStackTrace(e.getStackTrace());

					log.error("", iere);

					throw iere;
				} catch (InvalidExcelRowException e) {
					throw e;
				}

			}

			// Writing data to excel file
			if (!writeExcelSheet(conDesc)) {
				return false;
			}
			closeExcelSheet(conDesc);

			log.info("[ Updated excel row : " + excelTable + " ]");
		}

		if (columnsNotUpdated.size() > 0) {
			log.debug("[ Columns that are not updated : " + columnsNotUpdated + " ]");
			log.debug("\nProbable reasons could be:" + "\n- You have specified an incorrect header row."
					+ "\n- There isn't a field mapped to these column names in VO object."
					+ "\n- If mapping exists, field name may be different from excel column name. or is non-public field."
					+ "\n- Column names may have special characters other than Alphanumeric characters, Underscore(_), Hyphen(-) and Space( ).");
		}

		return true;
	}

	/**
	 * Returns the total no. of rows in the sheet.
	 * 
	 * @param conDesc
	 *            {@link com.library.datasource.connectiondescriptors.ExcelConnectionDescriptor
	 *            ExcelConnectionDescriptor} instance.
	 * @return
	 * @throws IOException
	 */
	public static int getRowCount(ExcelConnectionDescriptor conDesc) throws IOException {
		log.info("Connecting to the excel file...");

		int count;
		synchronized (TOKEN) {
			readExcelSheet(conDesc);

			/*
			 * Even if sheet has no rows, getLastRowNum() returns the index of
			 * first row. Hence, using getPhysicalNumberOfRows() to ensure that
			 * 0 is returned if no rows exist. Also, +1 because getLastRowNum()
			 * gets the index of last row.
			 */
			count = (sheet.getPhysicalNumberOfRows() == 0 ? sheet.getPhysicalNumberOfRows()
					: sheet.getLastRowNum() + 1);

			closeExcelSheet(conDesc);
		}

		return count;
	}

	// ----------------------------------------------------------------------------------------------------

	/**
	 * Reads excel and loads the sheet. <br>
	 * Note: Make sure to close the connection using {@link #closeExcelSheet()}
	 * once done.
	 * 
	 * @param conDesc
	 *            {@link library.datasource.connectiondescriptors.ExcelConnectionDescriptor
	 *            ExcelConnectionDescriptor} instance
	 * @return true if successfully connected to the sheet. Exception is thrown
	 *         otherwise.
	 * @throws IOException
	 */
	private static boolean readExcelSheet(ExcelConnectionDescriptor conDesc) throws IOException {
		try {
			// Opening file
			try {
				log.debug("Creating stream to read excel file...");

				fileInputStream = new FileInputStream(conDesc.getExcelFilePath());
			} catch (FileNotFoundException e) {
				FileNotFoundException fnfe = new FileNotFoundException(
						"Invalid excel file path specified or file already open.");
				fnfe.setStackTrace(e.getStackTrace());

				throw fnfe;
			}

			log.debug("Reading the workbook...");
			// Reading file as workbook
			workbook = new XSSFWorkbook(fileInputStream);

			log.info("Attempting to search the sheet by name (Sheet Name: " + conDesc.getSheetName() + ")");
			// Reading sheet by Name or Index
			sheet = workbook.getSheet(WorkbookUtil.createSafeSheetName(conDesc.getSheetName()));

			// This will be the case when sheet name specified is wrong.
			if (sheet == null) {
				log.warn("Wrong name specified for the sheet. Ensure that sheet name specified is corrent");

				log.info("Attempting to search the sheet by index (Sheet Index: " + conDesc.getSheetIndex() + ")");
				sheet = workbook.getSheetAt(conDesc.getSheetIndex());
			}
			// This will be the case when there isn't even any sheet at the
			// specified index.
			if (sheet == null) {
				IOException ioe = new IOException(
						"Couldn't find the sheet specified. Ensure that either sheet name or index is correct.");
				throw ioe;
			}

		} catch (IOException e) {
			log.error("", e);
			throw e;
		}

		return true;
	}

	/**
	 * Opens a connection to the excel sheet. <br>
	 * Note: Make sure to close the connection using {@link #closeExcelSheet()}
	 * once done.
	 * 
	 * @param conDesc
	 *            {@link library.datasource.connectiondescriptors.ExcelConnectionDescriptor
	 *            ExcelConnectionDescriptor} instance
	 * @return true if successfully connected to the sheet. Exception is thrown
	 *         otherwise.
	 * @throws IOException
	 */
	private static boolean writeExcelSheet(ExcelConnectionDescriptor conDesc) throws IOException {

		try {
			// Opening file
			try {
				log.debug("Creating stream to write to excel file...");

				fileOutputStream = new FileOutputStream(conDesc.getExcelFilePath());
			} catch (FileNotFoundException e) {
				FileNotFoundException fnfe = new FileNotFoundException(
						"Invalid excel file path specified or file already open.");
				fnfe.setStackTrace(e.getStackTrace());

				throw fnfe;
			}

			log.debug("Writing to the excel file");
			workbook.write(fileOutputStream);

		} catch (IOException e) {
			log.error("", e);
			throw e;
		}

		return true;
	}

	/**
	 * Closes the connection to sheet which is opened using
	 * {@link #readExcelSheet(ExcelConnectionDescriptor)}
	 * 
	 * @return true if connection to sheet is successfully closed. Throws
	 *         exception otherwise
	 * @throws IOException
	 */
	private static boolean closeExcelSheet(ExcelConnectionDescriptor conDesc) throws IOException {
		try {
			log.debug("Attempting to close the workbook");
			if (workbook != null) {
				workbook.close();
				workbook = null;
			}

			log.debug("Attempting to close the excel file: " + conDesc.getExcelFilePath());
			if (fileInputStream != null) {
				fileInputStream.close();
				fileInputStream = null;
			}

			if (fileOutputStream != null) {
				fileOutputStream.close();
				fileOutputStream = null;
			}
		} catch (IOException e) {
			IOException ioe = new IOException("Couldn't close connect to excel file: '" + conDesc.getExcelFilePath()
					+ "'. Further actions on this file may fail." + "\n Close the file if already open.");
			ioe.setStackTrace(e.getStackTrace());

			log.error("", ioe);

			throw ioe;
		}

		return true;
	}
}
