package library.engines;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Properties;

import library.datasource.DataSourceHelper;
import library.datasource.connectiondescriptors.PropertiesConnectionDescriptor;
import library.logger.MessageLogger;

public class PropertiesSourceEngine {

	private static final MessageLogger log = new MessageLogger(PropertiesSourceEngine.class);
	// Object to be used by synchronized block
	private static String TOKEN = "TOKEN";

	/**
	 * Fetches all the properties [Keys, Values] from properties file. <br>
	 * Note: This method is thread safe.
	 * 
	 * @param conDesc
	 *            {@link library.datasource.connectiondescriptors.PropertiesConnectionDescriptor PropertiesConnectionDescriptor}
	 *            instance that describes file details.
	 * @return HashMap object containing Property name, Property Value pair read from an properties file.
	 *         Property names are normalized using {@link library.datasource.DataSourceHelper#normalizeHeaderName(String)
	 *         normalizeHeaderName(String)} method.
	 * @throws IOException
	 * @throws IllegalArgumentException
	 */
	public static HashMap<String, Object> loadData(PropertiesConnectionDescriptor conDesc) throws IOException,
			IllegalArgumentException, NullPointerException {

		Properties propertiesTable;
		FileInputStream fileInStream = null;
		HashMap<String, Object> dataSet = new HashMap<String, Object>();

		log.info("Loading data from properties file...");

		synchronized (TOKEN) {

			try {
				fileInStream = new FileInputStream(conDesc.getFilePath());
				propertiesTable = new Properties();
				propertiesTable.load(fileInStream);
			}
			catch (IllegalArgumentException e) {
				IllegalArgumentException iae = new IllegalArgumentException(
						"Properties file has invalid Unicode format." + conDesc.describeConnection(), e);
				throw iae;
			}
			catch (IOException e) {
				IOException ioe = new IOException("Error while reading the properties file."
						+ conDesc.describeConnection(), e);
				throw ioe;
			}
			finally {
				if (fileInStream != null)
					fileInStream.close();
			}

			log.debug("Filling data to external source DataSet");
			for (String key : propertiesTable.stringPropertyNames()) {
				dataSet.put(DataSourceHelper.normalizeHeaderName(key), propertiesTable.get(key));
			}
		}
		log.info("[ Data fetched from properties file : " + dataSet + " ]");

		return dataSet;
	}

	public static boolean saveData(HashMap<String, Object> dataSet, PropertiesConnectionDescriptor conDesc)
			throws IOException {

		Properties propertiesTable;
		FileInputStream fileInStream = null;
		FileOutputStream fileOutStream = null;
		// To track the properties that are not mapped
		ArrayList<String> propertiesNotUpdated;

		log.info("[ Storing data to properties file : " + dataSet + " ]");

		synchronized (TOKEN) {
			
			log.debug("Loading properties from properties file to match them to keys in dataset.");
			try {
				fileInStream = new FileInputStream(conDesc.getFilePath());
				propertiesTable = new Properties();
				propertiesTable.load(fileInStream);
			}
			catch (IllegalArgumentException e) {
				IllegalArgumentException iae = new IllegalArgumentException(
						"Properties file has invalid Unicode format." + conDesc.describeConnection(), e);
				throw iae;
			}
			catch (IOException e) {
				IOException ioe = new IOException("Error while reading the properties file."
						+ conDesc.describeConnection(), e);
				throw ioe;
			}
			finally {
				if (fileInStream != null)
					fileInStream.close();
			}

			log.debug("[ Current properties : " + propertiesTable + " ]");
			
			propertiesNotUpdated = new ArrayList<String>();

			log.debug("Updating properties values");
			for (String key : propertiesTable.stringPropertyNames()) {
				// Normalizing the property key value to match it with normalized keys in dataSet
				String normalizedKeyName = DataSourceHelper.normalizeHeaderName(key);

				if (dataSet.containsKey(normalizedKeyName)) {
					propertiesTable.setProperty(key, (String) dataSet.get(normalizedKeyName));
				}
				else {
					propertiesNotUpdated.add(key);
				}
			}
						
			log.debug("Storing properties to properties file.");
			try {
				fileOutStream = new FileOutputStream(conDesc.getFilePath());
				propertiesTable.store(fileOutStream, "Last updated:");

			}
			catch (IllegalArgumentException e) {
				IllegalArgumentException iae = new IllegalArgumentException(
						"Properties file has invalid Unicode format." + conDesc.describeConnection(), e);
				throw iae;
			}
			catch (IOException e) {
				IOException ioe = new IOException("Error while reading the properties file."
						+ conDesc.describeConnection(), e);
				throw ioe;
			}
			finally {
				if (fileOutStream != null)
					fileOutStream.close();
			}
			
			log.debug("[ Updated properties : " + propertiesTable + " ]");
		}

		if (propertiesNotUpdated.size() > 0) {
			log.debug("[ Properties that are not updated : " + propertiesNotUpdated + " ]");
			log.debug("\nProbable reasons could be:"
					+ "\n- You have specified an incorrect property key name."
					+ "\n- There isn't a field mapped to these property key in VO object."
					+ "\n- If mapping exists, field name may be different from property key name. or is non-public field."
					+ "\n- Key names may have special characters other than Alphanumeric characters, Underscore(_), Hyphen(-) and Space( ).");
		}

		return true;
	}
}
