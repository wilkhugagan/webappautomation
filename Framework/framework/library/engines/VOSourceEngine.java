package library.engines;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;

import library.datasource.DataSourceHelper;
import library.logger.MessageLogger;
import library.vo.ParentVO;

/**
 * 
 * @author Gagandeep Singh
 *
 */
public class VOSourceEngine {

	private static final MessageLogger log = new MessageLogger(VOSourceEngine.class);

	/**
	 * Populates the VO object's fields/variables whose corresponding columns exists in external data source. i.e., To read-in an
	 * external data source's column value against a column header/name; you must declare a public field in the VO class whose
	 * object you passed in. Otherwise the column in external data source will be ignored.
	 * 
	 * @param voObject
	 *            VO class's object that maps fields to the external data source's column headers
	 * @param dataSet
	 *            HashMap object containing Column Name, Column Value pair read from an external data source to store to VO
	 *            object. Column names must be normalized using
	 *            {@link library.datasource.DataSourceHelper#normalizeHeaderName(String) normalizeHeaderName(String)} method.
	 * @throws IllegalAccessException
	 *             will be thrown if trying to access a non-public field.
	 * @throws IllegalArgumentException
	 *             will be thrown if the type/format of data stored in excel doesn't match the type of field/variable in VO
	 *             class.
	 */
	public static void loadVOWithDataSet(ParentVO voObject, HashMap<String, Object> dataSet)
			throws IllegalAccessException, IllegalArgumentException {

		log.debug("Initiating data loading...");

		// To track the columns that are fetched
		ArrayList<String> voFieldNotLoaded = new ArrayList<String>();

		for (Field field : voObject.getClass().getFields()) {

			// Normalizing VO's field names to match it to the DataSet's normalized keys
			String normalizedFieldName = DataSourceHelper.normalizeHeaderName(field.getName());

			// To check if a column name is mapped to this field name
			if (dataSet.containsKey(normalizedFieldName)) {
				// As column names are mapped to the VO class's public data fields/variables,
				// keys(which are normalized and are table column/header names) are same as normalized field names.
				Object data = dataSet.get(normalizedFieldName);

				try {
					field.set(voObject, data);
				}
				catch (IllegalArgumentException e) {
					IllegalArgumentException iae = new IllegalArgumentException(
							"\nTrying to populate wrong type of data to '"
									+ voObject.getClass().getSimpleName()
									+ "."
									+ field.getName()
									+ "'"
									+ "\nActual type of external data source field: "
									+ data.getClass().getName()
									+ "."
									+ "\nNote that type of VO class field and external data source field must be same.");

					iae.setStackTrace(e.getStackTrace());

					log.error("", iae);

					throw iae;
				}
				catch (IllegalAccessException e) {
					IllegalAccessException iae = new IllegalAccessException("Unable to access '"
							+ voObject.getClass().getSimpleName() + "." + field.getName()
							+ "'. Please confirm it's a public field.");

					iae.setStackTrace(e.getStackTrace());

					log.error("", iae);

					throw iae;
				}
			}
			else {
				voFieldNotLoaded.add(field.getName());
			}
		}

		log.info("Data from external data source is loaded to " + voObject.getClass().getSimpleName()
				+ " object successfully.");

		log.info("[ " + voObject.getClass().getSimpleName() + " : " + voObject.listAllFields() + " ]");

		if (voFieldNotLoaded.size() > 0) {
			log.debug("[ VO fields that are not loaded : " + voFieldNotLoaded + " ]");
			log.debug("\nProbable reasons could be:"
					+ "\n- You have specified an incorrect header row/key name."
					+ "\n- VO field isn't mapped to the column/key."
					+ "\n- If mapping exists, field name may be different from column/key name. or is non-public field."
					+ "\n- Column/key names may have special characters other than Alphanumeric characters, Underscore(_), Hyphen(-) and Space( ).");
		}
	}

	/**
	 * Populates the external data source table object with the Field Name, value from the VO object. i.e., To save VO's field
	 * values to an external data source's; you must declare a public field in the VO class whose object you passed in. Otherwise
	 * the column in external data source will be ignored.
	 * 
	 * @param voObject
	 *            VO class's object that maps fields to the external data source's column headers.
	 * @param dataSet
	 *            Empty HashMap object to store Column Name, Column Value pair from VO object. Column names will be normalized
	 *            using {@link library.datasource.DataSourceHelper#normalizeHeaderName(String) normalizeHeaderName(String)} method.
	 * @throws IllegalAccessException
	 *             will be thrown if trying to access a non-public field.
	 */
	public static void unLoadVOToDataSet(ParentVO voObject, HashMap<String, Object> dataSet)
			throws IllegalAccessException {

		log.info("Initiating data unloading...");

		log.info("Unloading VO field values to the external data source table.");

		log.info("[ " + voObject.getClass().getSimpleName() + " : " + voObject.listAllFields() + " ]");

		for (Field field : voObject.getClass().getFields()) {

			try {
				if (field.get(voObject) != null) {
					String normalizedFieldName = DataSourceHelper.normalizeHeaderName(field.getName());

					// Adding Field Name, value pairs to the dataSet
					dataSet.put(normalizedFieldName, field.get(voObject));
				}
			}
			catch (IllegalAccessException e) {
				IllegalAccessException iae = new IllegalAccessException("Unable to access '"
						+ voObject.getClass().getSimpleName() + "." + field.getName()
						+ "'. Please confirm it's a public field.");

				iae.setStackTrace(e.getStackTrace());

				log.error("", iae);

				throw iae;
			}
		}
	}
}
