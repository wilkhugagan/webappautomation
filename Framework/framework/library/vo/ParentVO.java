package library.vo;

import java.util.HashMap;

/**
 * All the VO classes which loads data from external source through DAO's data sources are required to implement this interface.
 * 
 * @author Gagandeep Singh
 *
 */
public interface ParentVO {

	/**
	 * @return Field Name, Value pairs of all the fields/variables in the VO class.
	 */
	public HashMap<String, Object> listAllFields();
}
