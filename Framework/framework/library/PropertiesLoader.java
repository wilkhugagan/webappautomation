package library;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import library.logger.MessageLogger;

/**
 * This class is responsible to load properties/configurations
 * 
 * @author Gagandeep Singh
 */
public class PropertiesLoader {
	private static final MessageLogger log = new MessageLogger(PropertiesLoader.class); 
	private static final String CONF_FOLDER = System.getProperty("user.dir") + "/conf";
	private Properties prop;

	/**
	 * This method loads the properties in the specified .properties file name
	 * placed inside {@value #CONF_FOLDER}
	 * 
	 * @param propertiesFileName name of the properties file. ex: conf.properties, etc.
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public void load(String propertiesFileName) throws FileNotFoundException, IOException {
		String filePath = CONF_FOLDER + "/" + propertiesFileName;
		
		log.info("Loading properties from '" + filePath + "'");
		
		prop = new Properties();
		prop.load(new FileInputStream(filePath));
		
		log.info("Properties loaded.");
	}

	/**
	 * Returns the property value against the specified key.
	 * Note: Load properties (using {@link #load} method) before using this method.
	 * @param key
	 * @return
	 */
	public String getValue(String key) {
		if (prop == null) {
			log.error("Load properties before reading them.");
			return null;
		}
		else {
			return (prop.getProperty(key).equalsIgnoreCase("") ? null : prop.getProperty(key));
		}
	}
}
