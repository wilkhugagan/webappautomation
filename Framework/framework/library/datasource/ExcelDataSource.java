package library.datasource;

import java.io.IOException;
import java.util.HashMap;

import library.datasource.connectiondescriptors.ConnectionDescriptor;
import library.datasource.connectiondescriptors.ExcelConnectionDescriptor;
import library.engines.ExcelSourceEngine;
import library.engines.VOSourceEngine;
import library.exceptions.TransportFailureException;
import library.exceptions.excel.InvalidExcelCellStateException;
import library.exceptions.excel.InvalidExcelRowException;
import library.logger.MessageLogger;
import library.vo.ParentVO;

/**
 * 
 * @author Gagandeep Singh
 *
 */
public class ExcelDataSource implements DataSource {

	private static final MessageLogger log = new MessageLogger(ExcelDataSource.class);

	/**
	 * Loads the VO object passed with the data fetched from excel file.
	 * 
	 * @param voObject
	 *            One of the VO class instances.
	 * @param connectionDescriptorObject
	 *            one of {@link library.datasource.connectiondescriptors
	 *            Connection Descriptors} that corresponds to the type of
	 *            external data source.
	 * @return VO object
	 * @throws TransportFailureException
	 */
	@Override
	public void load(ParentVO voObject, ConnectionDescriptor connectionDescriptorObject)
			throws TransportFailureException {

		try {

			// Reading data in from excel sheet
			HashMap<String, Object> dataSet = null;
			try {
				log.info("Fetching data from " + connectionDescriptorObject.describeConnection());

				dataSet = ExcelSourceEngine.loadData((ExcelConnectionDescriptor) connectionDescriptorObject);
			} catch (IOException | InvalidExcelRowException | InvalidExcelCellStateException e) {
				throw e;
			}

			// Loading data read from excel to the VO object
			try {
				log.info("Loading " + voObject.getClass().getSimpleName() + " object with the excel data.");

				VOSourceEngine.loadVOWithDataSet(voObject, dataSet);
			} catch (IllegalArgumentException | IllegalAccessException e) {
				throw e;
			}
		} catch (Exception e) {
			TransportFailureException tfe = new TransportFailureException(
					"Failure while trying to load VO object from excel.", e);

			log.error("", tfe);

			throw tfe;
		}
	}

	@Override
	public boolean save(ParentVO voObject, ConnectionDescriptor connectionDescriptorObject)
			throws TransportFailureException {

		try {
			// Reading data in from excel sheet
			HashMap<String, Object> dataSet = new HashMap<String, Object>();

			// Unloading data from VO object to the DataSet
			try {
				log.info("Unloading " + voObject.getClass().getSimpleName() + " object to DataSet.");

				VOSourceEngine.unLoadVOToDataSet(voObject, dataSet);
			} catch (IllegalAccessException e) {
				throw e;
			}

			try {
				log.info("Saving data to " + connectionDescriptorObject.describeConnection());

				if (ExcelSourceEngine.saveData(dataSet, (ExcelConnectionDescriptor) connectionDescriptorObject)) {
					log.info("Data saved to excel successfully");
				} else {
					log.error("Unable to save data to excel");
				}
			} catch (IOException | InvalidExcelCellStateException e) {
				throw e;
			}

		} catch (Exception e) {
			TransportFailureException tfe = new TransportFailureException("Failure while trying to save data to excel.",
					e);

			log.error("", tfe);

			throw tfe;
		}

		return false;
	}

	/**
	 * Returns the total no. of rows in the sheet.
	 * 
	 * @param conDesc
	 *            {@link com.library.datasource.connectiondescriptors.ExcelConnectionDescriptor
	 *            ExcelConnectionDescriptor} instance.
	 * @return
	 * @throws TransportFailureException
	 */
	public int getRowCount(ExcelConnectionDescriptor conDesc) throws TransportFailureException {
		try {
			return ExcelSourceEngine.getRowCount(conDesc);
		} catch (Exception e) {
			TransportFailureException tfe = new TransportFailureException("Failure while trying to read no. of rows in the specified sheet.",
					e);

			log.error("", tfe);

			throw tfe;
		}
	}
}
