package library.datasource;

import library.logger.MessageLogger;

/**
 * 
 * @author Gagandeep Singh
 *
 */
public class DataSourceHelper {
	private static final MessageLogger log = new MessageLogger(DataSourceHelper.class);

	/**
	 * Use this method to normalize the column header name. i.e., Remove spaces, underscores(_) and hyphens(-) if any in the
	 * header name and change the case to UPPERCASE. <br>
	 * For example: FIRST_NAME or First-Name or First NAME will be changed to FIRSTNAME
	 * 
	 * @param fieldName
	 *            Name of the field/column
	 * @return Normalized name
	 */
	public static String normalizeHeaderName(String fieldName) {

		StringBuilder fieldNameSB = new StringBuilder(fieldName.toUpperCase());

		char[] charsToRemove = new char[] { ' ', '_', '-' };

		log.debug("Normalizing field name");
		for (int i = 0; i < fieldNameSB.length();) {
			if (fieldNameSB.charAt(i) == charsToRemove[0] || fieldNameSB.charAt(i) == charsToRemove[1]
					|| fieldNameSB.charAt(i) == charsToRemove[2]) {
				fieldNameSB.deleteCharAt(i);

				continue;
			}

			i++;
		}		
		
		log.debug("Before normalization: " + fieldName + "  |  After normalization: " + fieldNameSB.toString());

		return fieldNameSB.toString();
	}
}
