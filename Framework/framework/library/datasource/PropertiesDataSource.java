package library.datasource;

import java.io.IOException;
import java.util.HashMap;

import library.datasource.connectiondescriptors.ConnectionDescriptor;
import library.datasource.connectiondescriptors.PropertiesConnectionDescriptor;
import library.engines.PropertiesSourceEngine;
import library.engines.VOSourceEngine;
import library.exceptions.TransportFailureException;
import library.logger.MessageLogger;
import library.vo.ParentVO;

/**
 * 
 * @author Gagandeep Singh
 *
 */
public class PropertiesDataSource implements DataSource {

	private static final MessageLogger log = new MessageLogger(ExcelDataSource.class);

	@Override
	public void load(ParentVO voObject, ConnectionDescriptor connectionDescriptorObject)
			throws TransportFailureException {
		try {

			// Reading data in from properties file
			HashMap<String, Object> dataSet = null;
			try {
				log.info("Fetching data from " + connectionDescriptorObject.describeConnection());

				dataSet = PropertiesSourceEngine.loadData((PropertiesConnectionDescriptor) connectionDescriptorObject);
			} catch (IOException | IllegalArgumentException e) {
				throw e;
			}

			// Loading data read from properties file to the VO object
			try {
				log.info("Loading " + voObject.getClass().getSimpleName() + " object with the properties.");

				VOSourceEngine.loadVOWithDataSet(voObject, dataSet);
			} catch (IllegalArgumentException | IllegalAccessException e) {
				throw e;
			}
		} catch (Exception e) {
			TransportFailureException tfe = new TransportFailureException(
					"Failure while trying to load VO object from properties file.", e);

			log.error("", tfe);

			throw tfe;
		}
	}

	@Override
	public boolean save(ParentVO voObject, ConnectionDescriptor connectionDescriptorObject)
			throws TransportFailureException {

		try {
			HashMap<String, Object> dataSet = new HashMap<String, Object>();

			// Unloading data from VO object to the DataSet
			try {
				log.info("Unloading " + voObject.getClass().getSimpleName() + " object to DataSet.");

				VOSourceEngine.unLoadVOToDataSet(voObject, dataSet);
			} catch (IllegalAccessException e) {
				throw e;
			}

			try {
				log.info("Saving data to " + connectionDescriptorObject.describeConnection());

				if (PropertiesSourceEngine.saveData(dataSet, (PropertiesConnectionDescriptor) connectionDescriptorObject)) {
					log.info("Data saved to properties file successfully");
				}
				else
				{
					log.error("Unable to save data to properties file");
				}
				
			} catch (IOException e) {
				throw e;
			}
		} catch (Exception e) {
			TransportFailureException tfe = new TransportFailureException(
					"Failure while trying to save VO object value to properties file.", e);

			log.error("", tfe);

			throw tfe;
		}

		return false;
	}

}
