package library.datasource;

import library.datasource.connectiondescriptors.ConnectionDescriptor;
import library.exceptions.TransportFailureException;
import library.vo.ParentVO;

interface DataSource {

	/**
	 * This method loads the VO object with the data fetched from external data source (Excel, Oracle DB, Properties file, etc.). <br>
	 * Case 1: If say, external data source has columns that has no mapped field/variable in VO class then they're left with the
	 * default value of that type.<br>
	 * Case 2: Similarly, if there're isn't a column header(Named as field/variable) mapped for the field in the external data
	 * source then again that field is left with the default value for it's type.
	 * 
	 * @param voObject
	 *            VO class instance to populate
	 * @param connectionDescriptorObject
	 *            one of {@link library.datasource.connectiondescriptors Connection Descriptors} that corresponds to the type of
	 *            external data source.
	 * @throws TransportFailureException
	 */
	public void load(ParentVO voObject, ConnectionDescriptor connectionDescriptorObject) throws TransportFailureException;

	/**
	 * This method saves the VO object (field/variable values) to the external data source (Excel, Oracle DB, Properties file,
	 * etc.). <br>
	 * Note: This method does not take responsibility to create new column headers. It just populates the existing ones.
	 * Case 1: If say, external data source doesn't have a column that is mapped to the field/variable in VO class then that
	 * field value is skipped to be saved.<br>
	 * Case 2: Similarly, if there isn't a column header(Named as field/variable) mapped for the field in the external data
	 * source then again that field is skipped to be saved.
	 * 
	 * @param voObject
	 *            VO class instance with field/variables populated with values to store.
	 * @param connectionDescriptorObject
	 *            one of {@link library.datasource.connectiondescriptors Connection Descriptors} that corresponds to the type of
	 *            external data source.
	 * @return boolean true on success
	 * @throws TransportFailureException
	 */
	public boolean save(ParentVO voObject, ConnectionDescriptor connectionDescriptorObject) throws TransportFailureException;
}
