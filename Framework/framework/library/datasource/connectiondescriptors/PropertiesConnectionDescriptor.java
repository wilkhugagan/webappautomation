package library.datasource.connectiondescriptors;

import java.io.File;

import library.logger.MessageLogger;

/**
 * This class carries details that are required to make connection to a
 * Properties file.
 * 
 * @author Gagandeep Singh
 *
 */
public class PropertiesConnectionDescriptor implements ConnectionDescriptor {

	private MessageLogger log = new MessageLogger(ExcelConnectionDescriptor.class);
	private File propertiesFile;

	/**
	 * Initializes a PropertiesConnectionDescriptor object that represents a
	 * potential properties file connection.
	 * 
	 * @param propertiesFilePath
	 */
	public PropertiesConnectionDescriptor(String propertiesFilePath) {
		propertiesFilePath = propertiesFilePath.trim();

		if (!propertiesFilePath.endsWith(".properties")) {
			log.error("Invalid extension for Properties file. Only .properties extension must be used");
		}

		this.propertiesFile = new File(propertiesFilePath);
	}

	/**
	 * @return Description of the connection
	 */
	@Override
	public String describeConnection() {
		String desc = "\nProperties file:" + "\nLocation: " + this.getFilePath();

		return desc;
	}

	// Getters

	public String getFilePath() {
		return this.propertiesFile.getPath();
	}
}
