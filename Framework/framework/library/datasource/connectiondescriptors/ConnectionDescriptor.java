package library.datasource.connectiondescriptors;


public interface ConnectionDescriptor {
	
	/**
	 * @return String containing description of connection descriptor object
	 */
	public String describeConnection();
}
