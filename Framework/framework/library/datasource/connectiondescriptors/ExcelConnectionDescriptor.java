package library.datasource.connectiondescriptors;

import java.io.File;

import library.exceptions.excel.InvalidExcelRowException;
import library.logger.MessageLogger;

import org.apache.poi.ss.util.WorkbookUtil;

/**
 * This class carries details that are required to make connection to an Excel
 * sheet and specifies what fields to be fetched from or store back to the
 * sheet.
 * 
 * @author Gagandeep Singh
 *
 */
public class ExcelConnectionDescriptor implements ConnectionDescriptor {

	private MessageLogger log = new MessageLogger(ExcelConnectionDescriptor.class);
	private File excelFile;
	private String sheetName;
	private int sheetIndex;
	private int headerRowIndex;
	private int dataRowIndex = 1;

	/**
	 * Initializes an ExcelConnectionDescriptor object that represents a
	 * potential [Column Name, Column Value] set to fetch or store.
	 * 
	 * @param excelFilePath
	 *            Fully qualified path to the excel file to read from.
	 * @param sheetName
	 *            Name of the sheet in the excel file.
	 * @param sheetIndex
	 *            Index of the sheet in the excel file.<br>
	 *            Note: Although it's not a mandatory field to ask if you're
	 *            providing name of the sheet. But just in case sheet name
	 *            search fails, specifying index of the sheet proves to be a
	 *            robust input. <br>
	 *            So, if sheet search fails on searching by name. Search will be
	 *            performed by index.
	 * @param headerRowIndex
	 *            Represents the header row index(0 based) of the data. i.e.,
	 *            the row that contains the name for the data. This value shall
	 *            be smaller than dataRowIndex.
	 * @param dataRowIndex
	 *            Represents the row index(0 based) for the data values.
	 */
	public ExcelConnectionDescriptor(String excelFilePath, String sheetName, int sheetIndex, int headerRowIndex,
			int dataRowIndex) {

		excelFilePath = excelFilePath.trim();

		if ((!excelFilePath.endsWith(".xls")) && (!excelFilePath.endsWith(".xlsx"))) {
			log.error("Invalid extension for excel file. Only .xls and .xlsx file extensions must be used.");
		}

		this.excelFile = new File(excelFilePath);
		this.sheetName = WorkbookUtil.createSafeSheetName(sheetName);
		this.sheetIndex = sheetIndex;
		this.headerRowIndex = headerRowIndex;
		this.dataRowIndex = dataRowIndex;

		// Row that contains data should be later in the sequence than column
		// name row
		if (headerRowIndex >= dataRowIndex) {
			log.warn("Row number to fetch data from should be greater than table header row number.",
					new InvalidExcelRowException(
							"Row number to fetch data from should be greater than table header row number.", this));
		}
	}

	/**
	 * Initializes an ExcelConnectionDescriptor object that represents a
	 * potential [Column Name, Column Value] set to fetch or store. <br>
	 * Note: Data row is set to value 1 by default. So, use this constructor to
	 * store data. As, excel data source store method is written to handle the
	 * new data row creation.
	 * 
	 * @param excelFilePath
	 *            Fully qualified path to the excel file to read from.
	 * @param sheetName
	 *            Name of the sheet in the excel file.
	 * @param sheetIndex
	 *            Index of the sheet in the excel file.<br>
	 *            Note: Although it's not a mandatory field to ask if you're
	 *            providing name of the sheet. But just in case sheet name
	 *            search fails, specifying index of the sheet proves to be a
	 *            robust input. <br>
	 *            So, if sheet search fails on searching by name. Search will be
	 *            performed by index.
	 * @param headerRowIndex
	 *            Represents the header row index(0 based) of the data. i.e.,
	 *            the row that contains the name for the data. This value shall
	 *            be smaller than dataRowIndex.
	 */
	public ExcelConnectionDescriptor(String excelFilePath, String sheetName, int sheetIndex, int headerRowIndex) {
		excelFilePath = excelFilePath.trim();

		if ((!excelFilePath.endsWith(".xls")) && (!excelFilePath.endsWith(".xlsx"))) {
			log.error("Invalid extension for excel file. Only .xls and .xlsx file extensions must be used.");
		}

		this.excelFile = new File(excelFilePath);
		this.sheetName = WorkbookUtil.createSafeSheetName(sheetName);
		this.sheetIndex = sheetIndex;
		this.headerRowIndex = headerRowIndex;

		// Row that contains data should be later in the sequence than column
		// name row
		if (headerRowIndex >= dataRowIndex) {
			log.warn("Row number to store/fetch data to/from should be greater than table header row number.",
					new InvalidExcelRowException(
							"Row number to store/fetch data to/from should be greater than table header row number.",
							this));
		}
	}

	/**
	 * @return Description of the connection
	 */
	@Override
	public String describeConnection() {
		String desc = " excel file:" + "\n Location: " + this.getExcelFilePath() + "\n Sheet name: " + sheetName
				+ " at index: " + sheetIndex + "\n Header row index: " + headerRowIndex + "\n Data row index: "
				+ dataRowIndex;

		return desc;
	}

	// Getters

	public String getSheetName() {
		return this.sheetName;
	}

	/**
	 * Gets the index of the Sheet.
	 * 
	 * @return 0 based index of the sheet
	 */
	public int getSheetIndex() {
		return this.sheetIndex;
	}

	public String getExcelFilePath() {
		return this.excelFile.getPath();
	}

	/**
	 * Gets the index of the header/column names row.
	 * 
	 * @return 0 based index of the header/column names row
	 */
	public int getTableHeaderRowIndex() {
		return this.headerRowIndex;
	}

	/**
	 * Gets the index of the data row.
	 * 
	 * @return 0 based index of the data row
	 */
	public int getDataRowIndex() {
		return this.dataRowIndex;
	}

	// Setters

	/**
	 * Sets the index of the row that contains data.
	 * 
	 * @param rowIndex
	 *            0 based index value of the row
	 */
	public void setDataRowIndex(int rowIndex) {
		this.dataRowIndex = rowIndex;

		// Row that contains data should be later in the sequence than column
		// name row
		if (headerRowIndex >= this.dataRowIndex) {
			log.warn("Row number that contains data should be greater than header row number.",
					new InvalidExcelRowException(
							"Row number that contains data should be greater than header row number.", this));
		}
	}
}
