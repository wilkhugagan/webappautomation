package library.datasource;


public enum DataSourceType {
	EXCEL, ORACLE_DB, PROPERTIES_FILE;	
}
