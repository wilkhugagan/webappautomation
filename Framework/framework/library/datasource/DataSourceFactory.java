package library.datasource;

import library.logger.MessageLogger;

public class DataSourceFactory {
	private static final MessageLogger log = new MessageLogger(DataSourceFactory.class);

	public static DataSource getDataSource(DataSourceType dataSourceType) {

		if (dataSourceType == DataSourceType.EXCEL) {
			log.debug("Creating a new Excel Data Source instance.");
			return new ExcelDataSource();
		}

		else if (dataSourceType == DataSourceType.PROPERTIES_FILE) {
			log.debug("Creating a new Properties File Data Source instance.");
			return new PropertiesDataSource();
		}

		return null;
	}
}
