package library.logger;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.ConsoleAppender;
import org.apache.log4j.FileAppender;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.log4j.PatternLayout;

/**
 * This class allows you to write log messages by providing you different methods to log them. It uses the log4j 1.2 library for
 * logging.
 * 
 * @author Gagandeep Singh
 *
 */
public class MessageLogger {

	// To keep track of execution state
	private static boolean isExecutionInProgressNow = false;

	// To check if the runtime arguments contains the parameter to run JVM in debugging mode
	boolean isDebugModeOn = java.lang.management.ManagementFactory.getRuntimeMXBean().getInputArguments().toString()
			.contains("-agentlib:jdwp");
	private final Logger log;
	private static ConsoleAppender consoleAppender = null;
	private static FileAppender fileAppender = null;
	private static PatternLayout layout = null;
	private static String logFileLocation = null;
	// Constants
	private static final String CONFIG_FILE_LOCATION = System.getProperty("user.dir") + "/conf/Log4jConfig.properties";
	private static final String DEFAULT_LOG_FILE_LOCATION = "C:\\logs\\selenium.log";
	private static final String DEFAULT_LOG_PATTERN_LAYOUT = "%d{ISO8601} [%t] %-5p [ %c %x] : %m%n";
	private static final SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd_hh-mm-ss_a"); // Ex: 2016.07.08.12:32:58.PM

	/**
	 * Creates a new logger object that is bind with the class name you specified. Also, configures and adds a console and a file
	 * appender for the logger. <br>
	 * Note: It is advised to create a static final instance.
	 * 
	 * @param className
	 *            Class object of the class you want to bind your logger object to.
	 */
	public MessageLogger(Class<?> className) {
		// Creating new logger with the class's name it's instantiated from
		log = LogManager.getLogger(className);

		initializeParameters();

		configureLogger();
	}

	/**
	 * Initializes logger parameters from configuration file.
	 */
	private void initializeParameters() {		
		if (logFileLocation != null && layout != null)
			return;

		Properties prop = new Properties();
		try {
			prop.load(new FileInputStream(CONFIG_FILE_LOCATION));
		}
		catch (IOException e) {
			System.out.println("Using default parameters:" + "\nlog.file.path=" + DEFAULT_LOG_FILE_LOCATION
					+ "\nlog.pattern.layout=" + DEFAULT_LOG_PATTERN_LAYOUT);

			System.out.println("as" + "\nLogger config(" + CONFIG_FILE_LOCATION + ") file not found.");
			e.printStackTrace();
		}
		finally {
			logFileLocation = (prop.getProperty("log.file.path") != null ? prop.getProperty("log.file.path")
					: DEFAULT_LOG_FILE_LOCATION);
			layout = new PatternLayout(
					(prop.getProperty("log.pattern.layout") != null ? prop.getProperty("log.pattern.layout")
							: DEFAULT_LOG_PATTERN_LAYOUT));
		}

	}

	/**
	 * Initializes and adds the appenders to the logger
	 * 
	 * @throws IOException
	 *             Will be thrown in case logger is unable to create log file or destination directory
	 */
	private void configureLogger() {
		// Initializing Appenders
		initializeAppenders();

		// Adding appenders to the logger
		log.addAppender(consoleAppender);
		log.addAppender(fileAppender);
	}

	/**
	 * Method responsible to initialize the appenders
	 */
	private void initializeAppenders() {

		if (consoleAppender != null && fileAppender != null)
			return;

		try {
			consoleAppender = new ConsoleAppender(layout);
			fileAppender = new FileAppender(layout, this.getLogFile().getPath(), true);
		}
		catch (IOException ioe) {
			System.out
					.println("Unable to initialize file appender.\n What does this mean ? "
							+ "\n This means that no log messages will be written to the log file but only to the console output window."
							+ "\n See errors for more details ");
			System.err.println("Unable to initialize file appender.");
			ioe.printStackTrace(System.err);
		}
	}

	/**
	 * Use this method to log debug messages in debug mode only.
	 * 
	 * @param message
	 *            String message to log
	 */
	public void debug(String message) {

		// Debug level message will only be logged if the JVM is running in debugging mode
		if (isDebugModeOn)
			log.debug(message);
	}

	/**
	 * Use this method to log warning messages.
	 * 
	 * @param message
	 *            String message to log
	 */
	public void warn(String message) {
		log.warn(message);
	}

	/**
	 * Use this method to log warning messages with an exception that caused the error
	 * 
	 * @param message
	 *            String message to log
	 * @param e
	 *            Exception object
	 */
	public void warn(String message, Throwable e) {
		log.error(message, e);
	}

	/**
	 * Use this method to log info messages
	 * 
	 * @param message
	 *            String message to log
	 */
	public void info(String message) {
		log.info(message);
	}

	/**
	 * Use this method to log error messages
	 * 
	 * @param message
	 *            String message to log
	 */
	public void error(String message) {
		log.error(message);
	}

	/**
	 * Use this method to log error messages with an exception that caused the error
	 * 
	 * @param message
	 *            String message to log
	 * @param e
	 *            Exception object
	 */
	public void error(String message, Throwable e) {
		log.error(message, e);
	}

	/**
	 * Creates and return a new log file in case one does not exist.<br>
	 * On every new execution, existing log file is archived/renamed with time stamp in the end of the file name
	 * 
	 * @return File object of the log file
	 */
	private File getLogFile() {
		File file = new File(logFileLocation);

		if (!file.exists()) {
			try {
				file.getParentFile().mkdirs(); // Shall create parent
												// directories if not exists

				System.out.println("Creating log file at '" + file.getPath() + "'");
				file.createNewFile(); // Shall create the file
			}
			catch (IOException e) {
				System.out.println("Error while create new log file.\n See error for more details");
				System.err.println("Error while create new log file.");
				e.printStackTrace(System.err);
			}
		}
		else if (file.exists() && (isExecutionInProgressNow == false)) {
			String archiveFileName = file.getPath() + "." + formatter.format(new Date());

			System.out.println("Archiving previous execution logs to file: '" + archiveFileName + "'");

			try {
				FileUtils.moveFile(file, new File(archiveFileName));

				// Recursively calling this method to create a new file this time.
				this.getLogFile();
			}
			catch (IOException e) {
				System.err.println("Unable to archive log file.\n Using log file at '" + file.getPath() + "'.\n"
						+ e.getMessage());
				e.printStackTrace(System.err);
			}
		}

		// Now as its a static variable, will be set to true on first call to
		// the getLogFile method and will remain true until
		// execution stops.
		isExecutionInProgressNow = true;

		return file;
	}

}
