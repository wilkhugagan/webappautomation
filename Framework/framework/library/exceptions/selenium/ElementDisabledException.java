package library.exceptions.selenium;

import org.openqa.selenium.WebElement;

/**
 * Use this class to notify the user about a disabled/inactive element.
 * 
 * @author Gagandeep Singh
 *
 */
public class ElementDisabledException extends Exception {

	private static final long serialVersionUID = 293436072634160611L;

	private final WebElement element;


	// Constructors

	/**
	 * Use this class to notify the user about a disabled/inactive element.
	 * 
	 * @param message
	 * @param element
	 */
	public ElementDisabledException(String message, WebElement element) {
		super(message);
		this.element = element;
	}


	// Getters and Setters

	public String getMessage() {
		return "\n Message: " + super.getMessage() + "\n Element: " + this.element.getText() + "\n Element Type: "
				+ this.element.getTagName();
	}
}
