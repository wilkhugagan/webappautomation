package library.exceptions.selenium;

import org.openqa.selenium.WebElement;

/**
 * Use this class to restrict/notify user about an invalid value entered or selected.
 * 
 * @author Gagandeep Singh
 *
 */
public class InvalidValueSelectionException extends Exception {

	private static final long serialVersionUID = -7136941358224943835L;

	private final WebElement element;


	// Constructors
	/**
	 * Use this class to restrict/notify user about an invalid value entered or selected.
	 * 
	 * @param message
	 * @param element
	 */
	public InvalidValueSelectionException(String message, WebElement element) {
		super(message);
		this.element = element;
	}


	// Getters and Setters

	public String getMessage() {
		return "\n Message: " + super.getMessage() + "\n Element: " + this.element.getText() + "\n Element Type: "
				+ this.element.getTagName();
	}
}
