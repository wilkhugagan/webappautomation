package library.exceptions.selenium;

import org.openqa.selenium.By;

/**
 * Use this class to restrict/notify user when an invalid/incompatible element search method is used for the scenario.
 * 
 * @author Gagandeep Singh
 *
 */
public class IncompatibleSearchMethodException extends Exception {

	private static final long serialVersionUID = 7022096388281389221L;

	private final By searchBy;

	// Constructors

	/**
	 * Use this class to restrict/notify user when an invalid/incompatible {@link org.openqa.selenium.By By} is used for the scenario.
	 * 
	 * @param message
	 * @param searchBy
	 */
	public IncompatibleSearchMethodException(String message, By searchBy) {
		super(message);
		this.searchBy = searchBy;
	}

	// Getters and Setters

	public String getMessage() {
		return "\n Message: " + super.getMessage() + "\n Search method: " + this.searchBy;
	}
}
