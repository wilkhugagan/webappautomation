package library.exceptions;

/**
 * Use this class to raise exceptions for failures while trying to execute a command or a file.
 * 
 * @author Gagandeep Singh
 *
 */
public class CommandFailureException extends Exception {
	private static final long serialVersionUID = -3000988987749137878L;

	public CommandFailureException(String message) {
		super(message);
	}

	public CommandFailureException(String message, Throwable e) {
		super(message, e);
	}
}
