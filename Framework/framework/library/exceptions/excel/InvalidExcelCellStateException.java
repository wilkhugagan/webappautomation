package library.exceptions.excel;

import library.datasource.connectiondescriptors.ExcelConnectionDescriptor;

/**
 * Use this class to raise exception for instances like invalid cell data, invalid cell state, etc.
 * 
 * @author Gagandeep Singh
 *
 */
public class InvalidExcelCellStateException extends Exception {

	private static final long serialVersionUID = -8613534957481338036L;
	private ExcelConnectionDescriptor conDesc;
	private int cellIndex;	

	public InvalidExcelCellStateException(String message, ExcelConnectionDescriptor conDesc, int cellIndex) {
		super(message);
		this.conDesc = conDesc;
		this.cellIndex = cellIndex;
	}
	
	public String getMessage() {
		String msg = 
				super.getMessage()
				+ "\nAccessing excel file at '" + conDesc.describeConnection()
				+ "\nCell index: " + this.cellIndex;
		
		return msg;
	}
}
