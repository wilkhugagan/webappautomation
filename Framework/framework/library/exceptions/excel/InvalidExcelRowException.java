package library.exceptions.excel;

import library.datasource.connectiondescriptors.ExcelConnectionDescriptor;

/**
 * Use this class to raise exceptions for invalid row access by user
 * 
 * @author Gagandeep Singh
 *
 */
public class InvalidExcelRowException extends Exception {

	private static final long serialVersionUID = 1519739419467351366L;
	private ExcelConnectionDescriptor conDesc;
	
	/**
	 * Use this class to raise exceptions for invalid row access by user
	 * 
	 * @param message
	 * @param conDesc ExcelConnection instance
	 */
	public InvalidExcelRowException(String message, ExcelConnectionDescriptor conDesc){
		super(message);
		this.conDesc = conDesc;
	}
	
	public String getMessage() {
		String msg = super.getMessage() + this.conDesc.describeConnection();
		
		return msg;
	}
}
