package library.exceptions;

/**
 * Use this class to wrap exceptions generated while transferring data to and from the external data sources
 * 
 * @author Gagandeep Singh
 *
 */
public class TransportFailureException extends Exception {
	
	private static final long serialVersionUID = -8009412848909930435L;

	/**
	 * Use this class to wrap exceptions generated while transferring data to and from the external data sources
	 * 
	 * @param message
	 * @param e Exception to wrap
	 */
	public TransportFailureException(String message, Throwable e){
		super(message, e);
	}
	
	/**
	 * Use this class to wrap exceptions generated while transferring data to and from the external data sources
	 * 
	 * @param message
	 */
	public TransportFailureException(String message) {
		super(message);
	}
}
