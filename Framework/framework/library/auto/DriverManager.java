package library.auto;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.FluentWait;

import library.logger.MessageLogger;

/**
 * Any configuration changes to active driver object should be handled by this
 * class's static methods.
 * 
 * @author Gagandeep Singh
 *
 */
class DriverManager {

	private static final MessageLogger log = new MessageLogger(DriverManager.class);

	/**
	 * This method sets the window state to maximized size.
	 * 
	 * @param driver
	 *            WebDriver object to configure
	 */
	public static void maximizeWindow(WebDriver driver) {
		log.debug("Configuring Driver: Maximizing the browser window");
		driver.manage().window().maximize();
	}

	/**
	 * Refreshes the current page.
	 * 
	 * @param driver
	 *            WebDriver object to configure
	 */
	public static void refreshPage(WebDriver driver) {
		log.debug("Refreshing the page");
		driver.navigate().refresh();
	}

	/**
	 * Applies an implicit wait to the driver.
	 * 
	 * @param driver
	 *            WebDriver object to configure.
	 * @param timeOut
	 *            Wait until this many seconds to throw NoSuchElementException
	 */
	public static void configureImplicitWait(WebDriver driver, int timeOut) {
		log.debug("Configuring Driver: Setting an implicit wait of " + timeOut + " second(s)");
		driver.manage().timeouts().implicitlyWait(timeOut, TimeUnit.SECONDS);
	}

	/**
	 * Creates, configures and attaches a FluentWait object to the driver
	 * specified.
	 * 
	 * @param driver
	 *            WebDriver object to configure
	 * @param thatPollsEvery
	 *            Time to wait for before next attempt(in milliseconds)
	 * @param withTimeOut
	 *            Wait until this many seconds to throw NoSuchElementException
	 * @return Resulting new FluentWait<WebDriver> object
	 */
	public static FluentWait<WebDriver> attachFluentWaitTo(WebDriver driver, int thatPollsEvery, int withTimeOut) {
		log.debug("Configuring Driver: Creating a wait that polls every " + thatPollsEvery
				+ "milliseconds with timeout of " + withTimeOut + " second(s) to ignore NoSuchElementException");

		return (new FluentWait<WebDriver>(driver)).pollingEvery(thatPollsEvery, TimeUnit.MILLISECONDS)
				.withTimeout(withTimeOut, TimeUnit.SECONDS).ignoring(NoSuchElementException.class);
	}
}
