package library.auto;

import java.io.IOException;

import library.exceptions.CommandFailureException;
import library.logger.MessageLogger;

/**
 * This class allows you to run executable files/commands with or without parameter.
 * 
 * @author Gagandeep Singh
 *
 */
public class ExeFuncLibrary {

	private static final MessageLogger log = new MessageLogger(ExeFuncLibrary.class);

	/**
	 * Runs an executable file.
	 * 
	 * @param filePath Absolute path to executable.
	 * @throws CommandFailureException If something is wrong with executable file specified.
	 */
	public static void runExecutable(String filePath) throws CommandFailureException {
		runExecutable(filePath, null);
	}
	
	/**
	 * Runs an executable file with parameters in paramList object.
	 * 
	 * @param filePath Absolute path to executable.
	 * @param paramList String[] where each index contains one parameter to supply to executable.
	 * @throws CommandFailureException If something is wrong with executable file or the parameter list specified.
	 */
	public static void runExecutable(String filePath, String[] paramList) throws CommandFailureException {		
		try {
			execute(filePath, paramList);
		}
		catch(NullPointerException | IllegalArgumentException e) {
			CommandFailureException cfe = new CommandFailureException("Null/Empty file path supplied", e);
			throw cfe;
		}
		catch (IOException e) {
			CommandFailureException cfe = new CommandFailureException("I/O error while trying to run file: " + filePath, e);
			throw cfe;
		}
		catch (InterruptedException e) {
			CommandFailureException cfe = new CommandFailureException("Error while waiting for executable(" + filePath + ") to complete it's task", e);
			throw cfe;
		}
	}

	/**
	 * Executes the command with or without parameters. If paramList is null, only command is executed. Otherwise, command is
	 * executed with list of parameters in paramList.
	 * 
	 * @param command
	 * @param paramList String[] where each index contains one parameter to supply to command.
	 * @throws IOException If an I/O error occurs.
	 * @throws NullPointerException If command is null.
	 * @throws IllegalArgumentException If command is empty string.
	 * @throws InterruptedException
	 */
	private static void execute(String command, String[] paramList) throws IOException, NullPointerException, IllegalArgumentException, InterruptedException {

		if (paramList != null) {
			String _params = ""; // To print parameter list in one log entry
			String[] params = new String[paramList.length + 1];

			params[0] = command;
			for (int i = 1; i < params.length;) {
				_params += " " + paramList[i - 1];
				params[i] = paramList[i - 1];
				
				i++;
			}

			log.debug("Executing: " + command + "\nWith parameters: " + _params);

			Runtime.getRuntime().exec(params).waitFor();
		}
		else {
			log.debug("Executing: " + command);

			Runtime.getRuntime().exec(command).waitFor();
		}

	}
}
