package library.auto;

import library.auto.contants.Browsers;
import library.auto.contants.SelectMethod;
import library.auto.contants.TimeUnits;
import library.exceptions.selenium.IncompatibleSearchMethodException;
import library.exceptions.selenium.InvalidValueSelectionException;
import library.logger.MessageLogger;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.By.ByLinkText;
import org.openqa.selenium.By.ByPartialLinkText;
import org.openqa.selenium.By.ByTagName;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Select;

/**
 * This is the selenium library for the use of automation of tests. Methods
 * under this class are designed to allow you to automate the user actions.
 * 
 * @author Gagandeep Singh
 */
public final class SeleniumFuncLibrary {
	/**
	 * Stores the current browser for this object
	 */
	public final Browsers currentBrowser;
	private WebDriver driver;
	// Allows to perform different actions like click, hover, type on the active
	// elements.
	private Actions actionOnElement;
	private Alert alert;
	private static final MessageLogger log = new MessageLogger(SeleniumFuncLibrary.class);
	// For applying extended waiting time for the elements over and above
	// implicit wait defined under driverFactory.configuredriver().
	private static FluentWait<WebDriver> wait;
	// To be used for the FluentWait.
	private static final int DEFAULT_TIMEOUT_FOR_ELEMENT_SEARCH = 10;
	// To be used for the FluentWait.
	private static final int DEFAULT_POLLING_FOR_ELEMENT_SEARCH = 250;

	/**
	 * Initializes a new instance of DriverFuncLibrary to perform automated
	 * actions
	 * 
	 * @param browser
	 *            Browser name ({@link library.auto.contants.Browsers Browsers})
	 */
	public SeleniumFuncLibrary(String browserName) {
		Browsers browser = Browsers.convertToBrowsers(browserName);

		currentBrowser = browser;
		// Initializing and configures driver
		driver = DriverFactory.getDriver(browser);

		// Configuring driver
		DriverManager.maximizeWindow(driver);

		// Initializing other variables
		//// Action class object to perform actions on the elements
		actionOnElement = new Actions(driver);
		//// Fluent wait to be used for element search
		wait = DriverManager.attachFluentWaitTo(driver, DEFAULT_POLLING_FOR_ELEMENT_SEARCH,
				DEFAULT_TIMEOUT_FOR_ELEMENT_SEARCH);
	}

	// Methods for browser handling

	/**
	 * Opens a new instance of browser with an initial URL.
	 * 
	 * @param url
	 *            URL of Initial page to open.
	 */
	public void launchBrowser(String url) {
		log.info("Opening new browser instance with URL: " + url);
		// Opens up a new instance of browser
		driver.get(url);
	}

	/**
	 * Direct navigation to the specified URL.
	 * 
	 * @param url
	 *            URL to navigate to, from current page.
	 */
	public void navigateToUrl(String url) {
		log.info("Navigating to URL: " + url);
		driver.navigate().to(url);
	}

	/**
	 * Refreshes the current page of active tab.
	 */
	public void refreshPage() {
		log.info("Refreshing the current page");
		DriverManager.refreshPage(driver);
	}

	/**
	 * Closes the browser
	 */
	public void closeBrowser() {
		log.info("Attempting to close the browser.");
		if (driver != null) {
			// Closes browser window
			driver.quit();

			log.info("Browser closed");
		} else {
			log.info("No browser instance is open.");
		}
	}

	/**
	 * Use this method only to verify that an element with given identifier
	 * exists on the page. Note: No action will be performed on the element.
	 * 
	 * @param searchByHierarchy
	 *            Element identifiers ({@link org.openqa.selenium.By By}) in
	 *            hierarchy from top to bottom level.
	 * @param timeoutForSearch
	 *            For how long you want search to be performed before raising a
	 *            NoSuchElementException.
	 * @return boolean true if element was found. false otherwise.
	 */
	public boolean elementExist(By[] searchByHierarchy, int timeoutForSearch) {
		log.info("Checking if specified element exists on the current page");
		// Searching for the element
		WebElement element = HelperLibrary.searchElement(searchByHierarchy, timeoutForSearch, wait);

		if (element != null)
			return true;
		else
			return false;
	}

	/**
	 * Use this method only to verify that an element with given identifier
	 * exists on the page with default timeout of
	 * {@value #DEFAULT_TIMEOUT_FOR_ELEMENT_SEARCH} seconds. Note: No action
	 * will be performed on the element.
	 * 
	 * @param searchByHierarchy
	 *            Element identifiers ({@link org.openqa.selenium.By By}) in
	 *            hierarchy from top to bottom level.
	 * @return boolean true if element was found. false otherwise.
	 */
	public boolean elementExist(By[] searchByHierarchy) {
		return elementExist(searchByHierarchy, DEFAULT_TIMEOUT_FOR_ELEMENT_SEARCH);
	}

	/**
	 * Checks if the element has the text specified.
	 * 
	 * @param text
	 *            Text/value to search in the element
	 * @param element
	 *            Element to check text in
	 * @param timeoutForSearch
	 *            For how long you want search to be performed before raising a
	 *            NoSuchElementException.
	 * @return boolean true if element was found. false otherwise.
	 */
	private boolean textExist(String text, WebElement element, int timeoutForSearch) {
		log.info("Searching for the specified text : '" + text + "'");
		if (element.getText().toUpperCase().contains(text.toUpperCase())) {
			log.info("Body text value in element: " + element.getText().toUpperCase());
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Checks if the element has the text specified.
	 * 
	 * @param text
	 *            Text/value to search in the element
	 * @param searchByHierarchy
	 *            Element identifiers ({@link org.openqa.selenium.By By}) in
	 *            hierarchy from top to bottom level.
	 * @param timeoutForSearch
	 *            For how long you want search to be performed before raising a
	 *            NoSuchElementException.
	 * @return boolean true if element was found. false otherwise.
	 */
	public boolean textExist(String text, By[] searchByHierarchy, int timeoutForSearch) {
		WebElement element = HelperLibrary.searchElement(searchByHierarchy, timeoutForSearch, wait);
		return textExist(text, element, timeoutForSearch);
	}

	/**
	 * Checks if the element has the text specified with default timeout of
	 * {@value #DEFAULT_TIMEOUT_FOR_ELEMENT_SEARCH} seconds.
	 * 
	 * @param text
	 *            Text/value to search in the element
	 * @param searchByHierarchy
	 *            Element identifiers ({@link org.openqa.selenium.By By}) in
	 *            hierarchy from top to bottom level.
	 * @return boolean true if element was found. false otherwise.
	 */
	public boolean textExist(String text, By[] searchByHierarchy) {
		return textExist(text, searchByHierarchy, DEFAULT_TIMEOUT_FOR_ELEMENT_SEARCH);
	}

	/**
	 * Gets you the text value inside body of the element.
	 * 
	 * @param element
	 *            Element to check text in
	 * @param timeoutForSearch
	 *            For how long you want search to be performed before raising a
	 *            NoSuchElementException.
	 * @return String Text inside the body of the element
	 */
	private String getText(WebElement element, int timeoutForSearch) {
		log.info("Fetching the text value in the specified element");
		if (element != null) {
			log.info("Body text value in element: " + element.getText());
			return element.getText();
		} else {
			return null;
		}
	}

	/**
	 * Gets you the text value inside body of the element.
	 * 
	 * @param searchByHierarchy
	 *            Element identifiers ({@link org.openqa.selenium.By By}) in
	 *            hierarchy from top to bottom level.
	 * @param timeoutForSearch
	 *            For how long you want search to be performed before raising a
	 *            NoSuchElementException.
	 * @return String Text inside the body of the element
	 */
	public String getText(By[] searchByHierarchy, int timeoutForSearch) {
		WebElement element = HelperLibrary.searchElement(searchByHierarchy, timeoutForSearch, wait);
		return getText(element, timeoutForSearch);
	}

	/**
	 * Gets you the text value inside body of the element.
	 * 
	 * @param searchByHierarchy
	 *            Element identifiers ({@link org.openqa.selenium.By By}) in
	 *            hierarchy from top to bottom level.
	 * @return String Text inside the body of the element
	 */
	public String getText(By[] searchByHierarchy) {
		return getText(searchByHierarchy, DEFAULT_TIMEOUT_FOR_ELEMENT_SEARCH);
	}

	/**
	 * This method performs click action on the element specified.
	 * 
	 * @param element
	 *            to click on.
	 * @param timeoutForSearch
	 *            For how long you want search to be performed before raising a
	 *            NoSuchElementException.
	 * @return boolean true if element was found. false otherwise.
	 * @return
	 */
	private boolean click(WebElement element, int timeoutForSearch) {
		if (element != null) {
			log.info("Performing click action");

			// Perform the click action
			actionOnElement.moveToElement(element).click().perform();

			return true;
		} else
			return false;
	}

	/**
	 * This method allows you to perform click action on an element on the web
	 * page. You can use this method for elements like button, link, etc. <br>
	 * Can't be used for: Selecting drop down items
	 * 
	 * @param searchByHierarchy
	 *            Element identifiers ({@link org.openqa.selenium.By By}) in
	 *            hierarchy from top to bottom level.
	 * @param timeoutForSearch
	 *            For how long you want search to be performed before raising a
	 *            NoSuchElementException.
	 * @return boolean true if element was found. false otherwise.
	 */
	public boolean click(By[] searchByHierarchy, int timeoutForSearch) {
		WebElement element = HelperLibrary.searchElement(searchByHierarchy, timeoutForSearch, wait);
		return click(element, timeoutForSearch);
	}

	/**
	 * This method allows you to perform click action on an element on the web
	 * page with default timeout of {@value #DEFAULT_TIMEOUT_FOR_ELEMENT_SEARCH}
	 * seconds. You can use this method for elements like button, link, etc.
	 * <br>
	 * Can't be used for: Selecting drop down items
	 * 
	 * @param searchByHierarchy
	 *            Element identifiers ({@link org.openqa.selenium.By By}) in
	 *            hierarchy from top to bottom level.
	 * @return boolean true if element was found. false otherwise.
	 */
	public boolean click(By[] searchByHierarchy) {
		return click(searchByHierarchy, DEFAULT_TIMEOUT_FOR_ELEMENT_SEARCH);
	}

	/**
	 * Types text value in the element (Expected to be a field).
	 * 
	 * @param value
	 *            Value to type in to the field.
	 * @param element
	 * @param For
	 *            how long you want search to be performed before raising a
	 *            NoSuchElementException.
	 * @return boolean true if element was found. false otherwise.
	 */
	private boolean type(String value, WebElement element, int timeoutForSearch) {
		if (element != null) {
			log.info("Typing the text to the field");
			// Moves the focus to the element first and type in the value.
			actionOnElement.sendKeys(element, value).perform();
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Types text value parameter in the field referred by searchRef based on
	 * the method specified by searchMethod.
	 * 
	 * @param value
	 *            Value to type in to the field.
	 * @param searchByHierarchy
	 *            Element identifiers ({@link org.openqa.selenium.By By}) in
	 *            hierarchy from top to bottom level.
	 * @param timeoutForSearch
	 *            For how long you want search to be performed before raising a
	 *            NoSuchElementException.
	 * @return boolean true if element was found. false otherwise.
	 */
	public boolean type(String value, By[] searchByHierarchy, int timeoutForSearch) {
		// Searches the element and assigns it to element object
		WebElement element = HelperLibrary.searchElement(searchByHierarchy, timeoutForSearch, wait);
		return type(value, element, timeoutForSearch);
	}

	/**
	 * Types text value parameter in the field referred by searchRef based on
	 * the method specified by searchMethod with a default timeout for search of
	 * {@value #DEFAULT_TIMEOUT_FOR_ELEMENT_SEARCH} seconds.
	 * 
	 * @param value
	 *            Value to type in to the field.
	 * @param searchByHierarchy
	 *            Element identifiers ({@link org.openqa.selenium.By By}) in
	 *            hierarchy from top to bottom level.
	 * @return boolean true if element was found. false otherwise.
	 */
	public boolean type(String value, By[] searchByHierarchy) {
		// Calling method above with a default timeout value
		return type(value, searchByHierarchy, DEFAULT_TIMEOUT_FOR_ELEMENT_SEARCH);
	}

	/**
	 * This method allows you to move and point cursor on some element.
	 * 
	 * @param element
	 *            Element to move cursor to.
	 * @param timeoutForSearch
	 *            For how long you want search to be performed before raising a
	 *            NoSuchElementException.
	 * @return boolean true if element was found. false otherwise.
	 */
	private boolean moveCursorTo(WebElement element, int timeoutForSearch) {
		if (element != null) {
			log.info("Hovering cursor on the element");
			// Mouse movement for chrome doesn't work properly unless mouse is
			// out of the page window. Hence, using this method
			HelperLibrary.moveMouseOutOfPageWindow(driver);

			actionOnElement.moveToElement(element).perform();

			return true;
		} else
			return false;
	}

	/**
	 * This method allows you to move and point cursor on some element.
	 * 
	 * @param searchByHierarchy
	 *            Element identifiers ({@link org.openqa.selenium.By By}) in
	 *            hierarchy from top to bottom level.
	 * @param timeoutForSearch
	 *            For how long you want search to be performed before raising a
	 *            NoSuchElementException.
	 * @return boolean true if element was found. false otherwise.
	 */
	public boolean moveCursorTo(By[] searchByHierarchy, int timeoutForSearch) {
		WebElement element = HelperLibrary.searchElement(searchByHierarchy, timeoutForSearch, wait);
		return moveCursorTo(element, timeoutForSearch);
	}

	/**
	 * This method allows you to move and point cursor on some element with a
	 * default timeout for search of
	 * {@value #DEFAULT_TIMEOUT_FOR_ELEMENT_SEARCH} seconds.
	 * 
	 * @param searchByHierarchy
	 *            Element identifiers ({@link org.openqa.selenium.By By}) in
	 *            hierarchy from top to bottom level.
	 * @return boolean true if element was found. false otherwise.
	 */
	public boolean moveCursorTo(By[] searchByHierarchy) {
		return moveCursorTo(searchByHierarchy, DEFAULT_TIMEOUT_FOR_ELEMENT_SEARCH);
	}

	/**
	 * This method allows selection of a value in a drop down list. <br>
	 * You can also select multiple values (in case drop down allows it) by
	 * calling this method multiple times.
	 * 
	 * @param selectBy
	 *            one of the selection methods from (
	 *            {@link library.auto.contants.SelectMethod SelectMethod})
	 * @param value
	 *            Drop down value to select. i.e., A text value in case of
	 *            {@link library.auto.contants.SelectMethod#VALUE
	 *            SelectMethod.VALUE} or sequence no (starts from 1) of the
	 *            value in the drop down in case of
	 *            {@link library.auto.contants.SelectMethod#SEQUENCE
	 *            SelectMethod.SEQUENCE}
	 * @param element
	 *            Element to select value in.
	 * @param timeoutForSearch
	 *            For how long you want search to be performed before raising a
	 *            NoSuchElementException.
	 * @return boolean true if element was found. false otherwise.
	 */
	private boolean selectFromDropdown(SelectMethod selectBy, String value, WebElement element, int timeoutForSearch) {
		if (element != null) {
			Select dropdown = new Select(element);

			try {
				if (selectBy == SelectMethod.VALUE) {
					try {
						log.info("Selecting item '" + value + "' in the dropdown");

						dropdown.selectByVisibleText(value);
					} catch (NoSuchElementException e) {
						throw new InvalidValueSelectionException("Wrong value specified", element);
					}
				} else if (selectBy == SelectMethod.SEQUENCE) {
					int sequence;
					try {
						sequence = Integer.parseInt(value) - 1;

						log.info("Selecting item at '" + sequence + "' position in the dropdown");

						dropdown.selectByIndex(sequence);
					} catch (NumberFormatException | NoSuchElementException e) {
						throw new InvalidValueSelectionException("Wrong index number specified", element);
					}
				}
			} catch (InvalidValueSelectionException e) {
				log.error("Invalid dropdown item selection: " + e.getMessage(), e);
			}

			return true;
		} else
			return false;

	}

	/**
	 * This method allows selection of a value in a drop down list. <br>
	 * You can also select multiple values (in case dropdown allows it) by
	 * calling this method multiple times.
	 * 
	 * @param selectBy
	 *            one of the selection methods from (
	 *            {@link library.auto.contants.SelectMethod SelectMethod})
	 * @param value
	 *            Dropdown value to select. i.e., A text value in case of
	 *            {@link library.auto.contants.SelectMethod#VALUE
	 *            SelectMethod.VALUE} or sequence no (starts from 1) of the
	 *            value in the dropdown in case of
	 *            {@link library.auto.contants.SelectMethod#SEQUENCE
	 *            SelectMethod.SEQUENCE}
	 * @param searchByHierarchy
	 *            Element identifiers ({@link org.openqa.selenium.By By}) in
	 *            hierarchy from top to bottom level.
	 * @param timeoutForSearch
	 *            For how long you want search to be performed before raising a
	 *            NoSuchElementException.
	 * @return boolean true if element was found. false otherwise.
	 */
	public boolean selectFromDropdown(SelectMethod selectBy, String value, By[] searchByHierarchy,
			int timeoutForSearch) {
		WebElement element = HelperLibrary.searchElement(searchByHierarchy, timeoutForSearch, wait);
		return selectFromDropdown(selectBy, value, element, timeoutForSearch);
	}

	/**
	 * This method allows selection of a value in a drop down list with a
	 * default timeout for search of
	 * {@value #DEFAULT_TIMEOUT_FOR_ELEMENT_SEARCH} seconds. <br>
	 * You can also select multiple values (in case dropdown allows it) by
	 * calling this method multiple times.
	 * 
	 * @param selectBy
	 *            one of the selection methods from (
	 *            {@link library.auto.contants.SelectMethod SelectMethod})
	 * @param value
	 *            Dropdown value to select. i.e., A text value in case of
	 *            {@link library.auto.contants.SelectMethod#VALUE
	 *            SelectMethod.VALUE} or sequence no of the value in the
	 *            dropdown in case of
	 *            {@link library.auto.contants.SelectMethod#SEQUENCE
	 *            SelectMethod.SEQUENCE}
	 * @param searchByHierarchy
	 *            Element identifiers ({@link org.openqa.selenium.By By}) in
	 *            hierarchy from top to bottom level.
	 * @return boolean true if element was found. false otherwise.
	 */
	public boolean selectFromDropdown(SelectMethod selectBy, String value, By[] searchByHierarchy) {
		return selectFromDropdown(selectBy, value, searchByHierarchy, DEFAULT_TIMEOUT_FOR_ELEMENT_SEARCH);
	}

	/**
	 * This method allows de-selection of all values in a drop down list.
	 * 
	 * @param searchByHierarchy
	 *            Element identifiers ({@link org.openqa.selenium.By By}) in
	 *            hierarchy from top to bottom level.
	 * @param timeoutForSearch
	 *            For how long you want search to be performed before raising a
	 *            NoSuchElementException.
	 * @return boolean true if element was found. false otherwise.
	 */
	private boolean deselectAllFromDropdown(By[] searchByHierarchy, int timeoutForSearch) {
		WebElement element = HelperLibrary.searchElement(searchByHierarchy, timeoutForSearch, wait);

		if (element != null) {
			Select dropdown = new Select(element);

			log.info("De-selecting all the dropdown values");
			dropdown.deselectAll();

			return true;
		} else
			return false;
	}

	/**
	 * This method allows de-selection all the values in a drop down list with a
	 * default timeout for search of
	 * {@value #DEFAULT_TIMEOUT_FOR_ELEMENT_SEARCH} seconds.
	 * 
	 * @param searchByHierarchy
	 *            Element identifiers ({@link org.openqa.selenium.By By}) in
	 *            hierarchy from top to bottom level.
	 * @return boolean true if element was found. false otherwise.
	 */
	public boolean deselectAllFromDropdown(By[] searchByHierarchy) {
		return deselectAllFromDropdown(searchByHierarchy, DEFAULT_TIMEOUT_FOR_ELEMENT_SEARCH);
	}

	/**
	 * Creates a new tab and opens URL specified in it. <br>
	 * Note: Currently works fine only for Firefox (46.X)
	 * 
	 * @param url
	 *            URL of the web page to open
	 * @return boolean true if element was found. false otherwise.
	 */
	public boolean openNewTab(String url) {

		WebElement element = HelperLibrary.searchElement(new By[] { By.tagName("body") },
				DEFAULT_TIMEOUT_FOR_ELEMENT_SEARCH, wait);

		if (element != null) {
			log.info("Opening a new tab");
			// Sending shortcut key combination to create a new tab.
			// executeBrowserShortcut(new Keys[] { Keys.CONTROL }, "t");

			actionOnElement.moveToElement(element).keyDown(Keys.CONTROL).sendKeys("t").keyUp(Keys.CONTROL).perform();

			log.info("Opening url in the new tab: " + url);
			driver.navigate().to(url);

			return true;
		} else
			return false;
	}

	/**
	 * This method switches to the tab/window (towards right) of the browser by
	 * number of tabs passed in
	 * 
	 * @param numberOfTabs
	 *            No. of tabs you want to move(right) to from current tab.
	 * @return boolean true if element was found. false otherwise.
	 */
	public boolean switchToTabForwardBy(int numberOfTabs) {

		WebElement element = HelperLibrary.searchElement(new By[] { By.tagName("body") },
				DEFAULT_TIMEOUT_FOR_ELEMENT_SEARCH, wait);

		if (element != null) {
			pause(2, "Pause to allow actions to sync", TimeUnits.SECONDS);

			log.info("Switching focus to " + numberOfTabs + " tab(s) away from current tab");
			for (int i = numberOfTabs; i > 0;) {

				// Performing Ctrl + Tab key action to switch tabs by 1 at a
				// time
				actionOnElement.moveToElement(element).keyDown(Keys.CONTROL).sendKeys(Keys.TAB).keyUp(Keys.CONTROL)
						.perform();
				i--;
			}

			return true;
		} else
			return false;
	}

	/**
	 * This method switches to the tab/window(towards left) of the browser by
	 * number of tabs passed in
	 * 
	 * @param numberOfTabs
	 *            No. of tabs you want to move(left) to from current tab.
	 * @return boolean true if element was found. false otherwise.
	 */
	public boolean switchToTabBackwardBy(int numberOfTabs) {

		WebElement element = HelperLibrary.searchElement(new By[] { By.tagName("body") },
				DEFAULT_TIMEOUT_FOR_ELEMENT_SEARCH, wait);

		if (element != null) {
			pause(2, "Pause to allow actions to sync", TimeUnits.SECONDS);

			log.info("Switching focus to " + numberOfTabs + " tab(s) back from current tab");
			for (int i = numberOfTabs; i > 0;) {

				// Performing Ctrl + Tab key action to switch tabs by 1 at a
				// time
				actionOnElement.moveToElement(element).keyDown(Keys.CONTROL).keyDown(Keys.SHIFT).sendKeys(Keys.TAB)
						.keyUp(Keys.CONTROL).keyUp(Keys.SHIFT).perform();
				i--;
			}
			return true;
		} else
			return false;
	}

	/**
	 * Allows you to a close tab
	 * 
	 * @param tabNumber
	 *            Relative number of tab(on right side) to close from current
	 *            tab.
	 */
	public void closeTab(int tabNumber) {

		// Switching focus to the tab to close
		switchToTabForwardBy(tabNumber);

		log.info("Closing the tab");
		// Closing the tab
		driver.close();
		driver.switchTo().defaultContent();
	}

	/**
	 * This method allows you to switch to one of the frames on the current web
	 * page. <br>
	 * Use {@link #resetFocus() resetFocus()} method to come out of frame once
	 * you are in.
	 * 
	 * @param searchBy
	 *            {@link org.openqa.selenium.By By}. For example: By.id("id
	 *            value"), By.xpath( ".//input[@id, 'id value']")
	 * @param timeoutForSearch
	 *            For how long you want search to be performed before raising a
	 *            NoSuchElementException.
	 * @return boolean true if element was found. false otherwise.
	 */
	private boolean switchToFrame(By searchBy, int timeoutForSearch) {

		if (searchBy instanceof ByTagName || searchBy instanceof ByLinkText || searchBy instanceof ByPartialLinkText) {

			log.error("Search methods By.tagName, By.linkText and By.partialLinkText are not allowed for frame search",
					new IncompatibleSearchMethodException("Incompatible search method specified", searchBy));
		} else {
			WebElement frameElement = HelperLibrary.searchElement(new By[] { searchBy }, timeoutForSearch, wait);

			if (frameElement != null) {
				log.info("Switching focus to the frame " + searchBy);

				driver.switchTo().frame(frameElement);

				return true;
			}
		}

		return false;
	}

	/**
	 * This method allows you to switch to one of the frames on the current web
	 * page with a default timeout for search of
	 * {@value #DEFAULT_TIMEOUT_FOR_ELEMENT_SEARCH} seconds. <br>
	 * Use {@link #resetFocus() resetFocus()} method to come out of frame once
	 * you are in.
	 * 
	 * @param searchBy
	 *            {@link org.openqa.selenium.By By}. For example: By.id("id
	 *            value"), By.xpath( ".//input[@id, 'id value']")
	 * @return boolean true if element was found. false otherwise.
	 */
	public boolean switchToFrame(By searchBy) {
		return switchToFrame(searchBy, DEFAULT_TIMEOUT_FOR_ELEMENT_SEARCH);
	}

	/**
	 * This method allows you to switch to one of the frames on the current web
	 * page. Use {@link #resetFocus() resetFocus()} method to come out of frame
	 * once you are in. <br>
	 * Use {@link #resetFocus() resetFocus()} method to come out of frame once
	 * you are in.
	 * 
	 * @param frameIndex
	 *            0 based index of the frame.
	 */
	public void switchToFrame(int frameIndex) {
		log.info("Switching focus to the frame " + (frameIndex));

		driver.switchTo().frame(frameIndex);
	}

	/**
	 * Allows you to select either the first frame on the page, or the main
	 * document when focus is inside a frame already. <br>
	 * Note: Use this method to come out of a frame if the focus is already
	 * inside the frame.
	 */
	public void resetFocus() {
		log.info("Moving focus out of the frame");
		driver.switchTo().defaultContent();
	}

	/**
	 * Switches focus to the browser popup
	 */
	public void switchToPopUp() {
		// Just in case pop up takes time to show up
		pause(1, "Allowing pop-up window to open properly", TimeUnits.SECONDS);

		log.info("Switching to the browser pop up");
		alert = driver.switchTo().alert();
	}

	/**
	 * Clicks the accept/ok button once focus is set to the pop up.
	 * 
	 * @return boolean true if element was found. false otherwise.
	 */
	public boolean popUpAccept() {

		if (alert != null) {
			log.info("Clicking OK on pop-up");
			alert.accept();

			return true;
		} else {
			log.error("Switch focus to pop up before trying to click accept button on it.");

			return false;
		}

	}

	/**
	 * This method allows you to perform drag and drop of an item on the web
	 * page provided drag and drop is allowed for the respective elements.
	 * 
	 * @param searchByForElementToDropHierarchy
	 *            Element identifiers ({@link org.openqa.selenium.By By}) in
	 *            hierarchy from top to bottom level.
	 * @param searchByForWhereToDropHierarchy
	 *            Element identifiers ({@link org.openqa.selenium.By By}) in
	 *            hierarchy from top to bottom level.
	 * @param timeoutForSearch
	 *            For how long you want search to be performed before raising a
	 *            NoSuchElementException.
	 * @return boolean true if element was found. false otherwise.
	 */
	private boolean dragAndDrop(By[] searchByForElementToDropHierarchy, By[] searchByForWhereToDropHierarchy,
			int timeoutForSearch) {

		WebElement elementToDrag = HelperLibrary.searchElement(searchByForElementToDropHierarchy, timeoutForSearch,
				wait),
				elementWhereToDrop = HelperLibrary.searchElement(searchByForWhereToDropHierarchy, timeoutForSearch,
						wait);

		if (elementToDrag != null && elementWhereToDrop != null) {

			log.info("Dragging element '" + elementToDrag.getText() + "' to drop on element '"
					+ elementWhereToDrop.getText() + "'");

			// Performing actions to drag and drop
			actionOnElement.dragAndDrop(elementToDrag, elementWhereToDrop).perform();

			return true;
		} else
			return false;
	}

	/**
	 * This method allows you to perform drag and drop of an item on the web
	 * page provided drag and drop is allowed for the respective elements with a
	 * default timeout for search of
	 * {@value #DEFAULT_TIMEOUT_FOR_ELEMENT_SEARCH} seconds.
	 * 
	 * @param searchByForElementToDropHierarchy
	 *            Element identifiers ({@link org.openqa.selenium.By By}) in
	 *            hierarchy from top to bottom level.
	 * @param searchByForWhereToDropHierarchy
	 *            Element identifiers ({@link org.openqa.selenium.By By}) in
	 *            hierarchy from top to bottom level.
	 * @return boolean true if element was found. false otherwise.
	 */
	public boolean dragAndDrop(By[] searchByForElementToDropHierarchy, By[] searchByForWhereToDropHierarchy) {
		return dragAndDrop(searchByForWhereToDropHierarchy, searchByForWhereToDropHierarchy,
				DEFAULT_TIMEOUT_FOR_ELEMENT_SEARCH);
	}

	// --------------------------------------------------------------------------------------------------------------

	/**
	 * Pauses the execution for no. of seconds passed <br>
	 * Note: Always add a reason good enough to explain why do you need a pause.
	 * 
	 * @param duration
	 *            No. of seconds to pause for
	 * @param reasonToPause
	 *            Specify a reason for the pause
	 * @param timeUnit
	 *            Unit of duration ({@link library.auto.contants.TimeUnits
	 *            TimeUnits})
	 */
	public void pause(int duration, String reasonToPause, TimeUnits timeUnit) {
		HelperLibrary.pause(duration, reasonToPause, timeUnit);
	}
}
