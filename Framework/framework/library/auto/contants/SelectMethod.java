package library.auto.contants;

/**
 * Provides options for methods to be used for selecting values from a dropdown
 * web element
 * 
 * @author Gagandeep Singh
 *
 */
public enum SelectMethod {
	SEQUENCE, VALUE
}
