package library.auto.contants;

/**
 * Represents time units
 * 
 * @author Gagandeep Singh
 *
 */
public enum TimeUnits {
	MILLISECONDS, SECONDS;
}
