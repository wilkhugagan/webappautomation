package library.auto.contants;

import library.logger.MessageLogger;

/**
 * Browser names
 * 
 * @author Gagandeep Singh
 *
 */
public enum Browsers {
	FIREFOX, CHROME, INTERNET_EXPLORER, MICROSOFT_EDGE;
	
	private static final MessageLogger log = new MessageLogger(Browsers.class);
	
	/**
	 * Converts browser name string to Browsers object. Only if browser of similar name exists.
	 * Otherwise, will log an error to log file and return default browser. i.e., FIREFOX.
	 * 	
	 * @param browserName Name of the browser.
	 * @return Browsers object of the similar browser name.
	 */
	public static Browsers convertToBrowsers(String browserName) {
		// Default browser in case browserName doesn't exist.
		Browsers browser = FIREFOX;
		
		/* Making the browserName compatible to Browsers format.
		 * i.e.,1. It shouldn't contain padded spaces. 
		 * 		2. It should be in upper case letters.
		 * 		3. Space should be represented by underscore(_)
		*/
		browserName = browserName.trim().toUpperCase().replace(' ', '_');
		
		try {
			browser = Browsers.valueOf(browserName);
		}
		catch (IllegalArgumentException iae) {
			log.warn("Browser name you specified is incorrect. Therefore, selecting default browser. i.e., " + FIREFOX);
			log.error(iae.getMessage(), iae);
		}
		
		return browser;
	}
}
