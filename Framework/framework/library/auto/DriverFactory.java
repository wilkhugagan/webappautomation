package library.auto;

import java.io.IOException;

import org.openqa.selenium.Proxy.ProxyType;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.firefox.internal.ProfilesIni;
import org.openqa.selenium.ie.InternetExplorerDriver;

import library.PropertiesLoader;
import library.auto.contants.Browsers;
import library.logger.MessageLogger;

/**
 * This is a factory class for the use of framework classes. Remember that you
 * are not supposed to use it for automation of tests.
 * 
 * @author Gagandeep Singh
 *
 */
class DriverFactory {

	private static final MessageLogger log = new MessageLogger(DriverFactory.class);
	private static final String DRIVER_CONF_FILE = "driver.properties";
	// Constants
	private static final String FIREFOX_DRIVER_PROPERTY_NAME = "driver.firefox";
	private static final String CHROME_DRIVER_PROPERTY_NAME = "driver.chrome";
	private static final String IE_DRIVER_PROPERTY_NAME = "driver.ie";
	private static final String FIREFOX_PROFILE_PROPERTY_NAME = "driver.firefox.profile";
	
	/**
	 * Creates, configures and returns a new WebDriver instance. Returns the
	 * cached instance if already running.
	 * 
	 * @param browser
	 *            Browser name ({@link library.auto.contants.Browsers Browsers})
	 * @return A new WebDriver object
	 */
	protected static WebDriver getDriver(Browsers browser) {
		WebDriver driver = null;

		PropertiesLoader propLoader = new PropertiesLoader();
		
		try {
			propLoader.load(DRIVER_CONF_FILE);
		} catch (IOException e) {
			log.error("Error while loading the driver properties.", e);
			log.error("Aborting execution");
			System.exit(-1);
		}
		
		log.debug("Initializing " + browser.name() + " driver");
		
		if (browser == Browsers.FIREFOX) {

			if (propLoader.getValue(FIREFOX_DRIVER_PROPERTY_NAME) == null) {
				log.error(FIREFOX_DRIVER_PROPERTY_NAME + " not found in " + DRIVER_CONF_FILE + "file");
				log.error("Aborting execution");
				System.exit(-1);
			}
			
			if (propLoader.getValue(FIREFOX_PROFILE_PROPERTY_NAME) == null) {
				log.error(FIREFOX_PROFILE_PROPERTY_NAME + " value not found in " + DRIVER_CONF_FILE + "file");
				log.error("Aborting execution");
				System.exit(-1);
			}
			
			ProfilesIni profile = new ProfilesIni();
			FirefoxProfile myprofile = profile.getProfile(propLoader.getValue(FIREFOX_PROFILE_PROPERTY_NAME));
			myprofile.setPreference("browser.startup.homepage", "about:blank");
			myprofile.setPreference("startup.homepage_welcome_url", "about:blank");
			myprofile.setPreference("startup.homepage_welcome_url.additional", "about:blank");
			myprofile.setPreference("network.proxy.type", ProxyType.AUTODETECT.ordinal());

			// Telling the Selenium WebDriver the path to access firefox from
			System.setProperty("webdriver.gecko.driver", propLoader.getValue(FIREFOX_DRIVER_PROPERTY_NAME));

			driver = new FirefoxDriver(myprofile);
		}

		else if (browser == Browsers.CHROME) {
			if (propLoader.getValue(CHROME_DRIVER_PROPERTY_NAME) == null) {
				log.error(CHROME_DRIVER_PROPERTY_NAME + " value not found in " + DRIVER_CONF_FILE + "file");
				log.error("Aborting execution");
				System.exit(-1);
			}
			
			// Telling the Selenium WebDriver the path to access chrome driver
			// from
			System.setProperty("webdriver.chrome.driver", propLoader.getValue(CHROME_DRIVER_PROPERTY_NAME));
			driver = new ChromeDriver();
		}

		else if (browser == Browsers.INTERNET_EXPLORER) {
			if (propLoader.getValue(IE_DRIVER_PROPERTY_NAME) == null) {
				log.error(IE_DRIVER_PROPERTY_NAME + " value not found in " + DRIVER_CONF_FILE + "file");
				log.error("Aborting execution");
				System.exit(-1);
			}
			
			// Telling the Selenium WebDriver the path to access chrome driver
			// from
			System.setProperty("webdriver.ie.driver", propLoader.getValue(IE_DRIVER_PROPERTY_NAME));
			driver = new InternetExplorerDriver();
		}

		return driver;
	}
}
