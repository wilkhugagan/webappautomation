package library.auto;

import java.awt.AWTException;
import java.awt.Robot;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.Point;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.FluentWait;

import com.google.common.base.Function;

import library.auto.contants.TimeUnits;
import library.exceptions.selenium.ElementDisabledException;
import library.logger.MessageLogger;

/**
 * This class is primarily for the use of classes under library.auto.core. <br>
 * Be cautious to use it in your automated test cases.
 * 
 * @author Gagandeep Singh
 *
 */
class HelperLibrary {
	private static final MessageLogger log = new MessageLogger(HelperLibrary.class);

	/**
	 * This method searches the element in the hierarchy defined by searchBy
	 * array and returns the child element. For example: searchBy[0] :
	 * GreatGrandParent, searchBy[1] : GrantParent , ... , searchBy[n] : child.
	 * 
	 * @param searchBy[]
	 *            {@link org.openqa.selenium.By By} object. For example: By.id(
	 *            "id value"), By.xpath( ".//input[@id, 'id value']")
	 * @param timeoutForSearch
	 *            For how long you want search to be performed before raising a
	 *            NoSuchElementException.
	 * @param wait
	 *            Active and configured FluentWait object
	 */
	public static WebElement searchElement(By[] searchBy, int timeoutForSearch, FluentWait<WebDriver> wait) {

		WebElement element = null;

		try {

			wait.withTimeout(timeoutForSearch, TimeUnit.SECONDS);

			log.info("Searching for element '" + searchBy[0].toString() + "'");

			element = wait.until(new Function<WebDriver, WebElement>() {
				public WebElement apply(WebDriver driver) {
					return driver.findElement(searchBy[0]);
				}
			});

			// Searching for child elements to element above.
			for (int i = 1; i < searchBy.length; i++) {
				element = searchChildElement(searchBy[i], element, timeoutForSearch, wait);
			}

			if (!element.isEnabled()) {
				ElementDisabledException e = new ElementDisabledException("Element is disabled/inactive right now",
						element);
				throw e;
			} else if (!element.isDisplayed()) {
				ElementNotVisibleException e = new ElementNotVisibleException(
						"Element is hidden/not visible right now");
				throw e;
			}

		} catch (NoSuchElementException nse) {
			log.error("Could not find the element", nse);
		} catch (ElementDisabledException | ElementNotVisibleException e) {
			log.error(e.getMessage());
		} catch (TimeoutException toe) {
			log.error("Timeout while trying to find element", toe);
		}

		return element;
	}

	/**
	 * This method is responsible for searching an element on inside/relative a
	 * parent element using one of the Search Methods in By class and assigns
	 * the element object to "element".
	 * 
	 * @param searchBy
	 *            {@link org.openqa.selenium.By By} object. For example: By.id(
	 *            "id value"), By.xpath( ".//input[@id, 'id value']")
	 * @param parentElement
	 *            Element under which you want to search the child element.
	 * @param timeoutForSearch
	 *            For how long you want search to be performed before raising a
	 *            NoSuchElementException.
	 * @param wait
	 *            Active and configured FluentWait object
	 */
	private static WebElement searchChildElement(By searchBy, WebElement parentElement, int timeoutForSearch,
			FluentWait<WebDriver> wait) {

		WebElement childElement = null;

		try {

			wait.withTimeout(timeoutForSearch, TimeUnit.SECONDS);

			log.info("Searching for child element '" + searchBy + "' under parent " + parentElement.toString());

			childElement = wait.until(new Function<WebDriver, WebElement>() {
				public WebElement apply(WebDriver driver) {
					return parentElement.findElement(searchBy);
				}
			});

			if (!childElement.isEnabled()) {
				ElementDisabledException e = new ElementDisabledException("Element is disabled/inactive right now",
						childElement);
				throw e;
			} else if (!childElement.isDisplayed()) {
				ElementNotVisibleException e = new ElementNotVisibleException(
						"Element is hidden/not visible right now");
				throw e;
			}

		} catch (NoSuchElementException nse) {
			log.error("Could not find the child element", nse);

		} catch (ElementDisabledException | ElementNotVisibleException e) {
			log.error(e.getMessage());
		} catch (TimeoutException toe) {
			log.error("Timeout while trying to find child element", toe);
		}

		return childElement;
	}

	/**
	 * There'll be instances when mouse motions done by Actions class needs
	 * cursor to be positioned out of current page window. <br>
	 * One of the instances is use of Actions.moveToElement(...) in Chrome
	 * browser. Which requires mouse to be out of page window sight. So, you can
	 * use this method for such cases.
	 * 
	 * @param WebDriver
	 *            instance
	 */
	public static void moveMouseOutOfPageWindow(WebDriver driver) {
		try {
			pause(2, "Pause to allow actions to sync", TimeUnits.SECONDS);

			Point currentBrowserProsition = driver.manage().window().getPosition();

			Robot robot = new Robot();
			robot.mouseMove(currentBrowserProsition.getX() + 10, currentBrowserProsition.getY() + 10);

		} catch (AWTException e) {
			log.warn("Unable to move mouse out of sight", e);
		}
	}

	/**
	 * This method will move the mouse to the center point of the web page.
	 * 
	 * @param WebDriver
	 *            instance
	 */
	public static void moveMouseToCenterOfPage(WebDriver driver) {
		try {
			Point inPageWindowSight = new Point(driver.manage().window().getSize().width / 2,
					driver.manage().window().getSize().height / 2);

			Robot robot = new Robot();
			robot.mouseMove(inPageWindowSight.getX() - 5, inPageWindowSight.getY() - 5);

		} catch (AWTException e) {
			log.warn("Unable to move mouse out of sight", e);
		}
	}

	/**
	 * Pauses the execution for no. of seconds passed <br>
	 * Note: Always add a reason good enough to explain why do you need a pause.
	 * 
	 * @param forSeconds
	 *            No. of seconds to pause for
	 * @param reasonToPause
	 *            Specify a reason for the pause
	 * @param timeUnit
	 *            Unit of duration ({@link library.auto.contants.TimeUnits
	 *            TimeUnits})
	 */
	public static void pause(int duration, String reasonToPause, TimeUnits timeUnit) {

		try {

			if (timeUnit == TimeUnits.MILLISECONDS) {
				log.info("Applying a pause of " + duration + " milliseconds."
						+ (reasonToPause != null ? " Reason: " + reasonToPause : ""));
				Thread.sleep(duration);
			} else if (timeUnit == TimeUnits.SECONDS) {
				log.info("Applying a pause of " + duration + " second(s)"
						+ (reasonToPause != null ? " Reason: " + reasonToPause : ""));
				Thread.sleep(duration * 1000);
			}
		} catch (InterruptedException e) {
			log.warn("Error while applying the pause: " + e.getMessage());
		}
	}
}
