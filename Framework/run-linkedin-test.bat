D:
set PROJECT_PATH=D:\Dev\Workspace\repositories\webappautomation\Framework
set classpath=%PROJECT_PATH%\lib\Log4j\*;%PROJECT_PATH%\lib\POI\*;%PROJECT_PATH%\lib\Selenium\lib\*;%PROJECT_PATH%\lib\Selenium\*;%PROJECT_PATH%\bin
cd %PROJECT_PATH%
dir /S /B .\framework\*.java > sources.txt
javac -d .\bin @sources.txt
dir /S /B .\test\*.java > sources.txt
javac -d .\bin @sources.txt
del sources.txt
java org.testng.TestNG testng.xml